#!/usr/bin/env pwsh
param ([switch]$bless)

Set-StrictMode -Version "latest"
$ErrorActionPreference = "Stop"

function CheckExitCode {
    if ($LASTEXITCODE -ne 0) {
        exit $LASTEXITCODE
    }
}

Push-Location "$PSScriptRoot/TsGen.Tests/typescript/"

if ($bless) {
    $env:UPDATE_SNAPSHOT = "1"
}

try {
    corepack pnpm install --frozen-lockfile
    CheckExitCode
    # workaround for https://github.com/dotnet/sdk/issues/29543
    $env:DOTNET_CLI_UI_LANGUAGE = "en-US"
    dotnet test ../
    CheckExitCode

    corepack pnpm run build
    CheckExitCode

    corepack pnpm run test
    CheckExitCode
}
finally {
    $env:UPDATE_SNAPSHOT = $null
    Pop-Location
}
