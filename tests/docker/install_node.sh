#!/usr/bin/env bash

set -euxo pipefail

FNM_PATH="$HOME/.local/share/fnm"

curl -fsSL https://fnm.vercel.app/install | bash -s -- --install-dir "$FNM_PATH" --skip-shell

PATH="$FNM_PATH:$PATH"
eval "$(fnm env --shell=bash)"

fnm install 22
node -v
corepack enable pnpm

# so that node and fnm are found when running container
cat >>"$HOME/.bashrc" <<EOF
PATH="$FNM_PATH:\$PATH"
eval "\$(fnm env --shell=bash)"
EOF
