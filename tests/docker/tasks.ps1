#!/usr/bin/env pwsh
param([switch]$build, [switch]$publish, [switch]$runTests)

$tag = "registry.gitlab.com/berublan/tsgen"

if ($build) {
    docker build $PSScriptRoot -t $tag
}

if ($publish) {
    docker push $tag
}

if ($runTests) {    
    git -C "$PSScriptRoot/../../" ls-files |
    tar --create --files-from - --gzip --to-stdout |
    # -l added to bash so that ~/.bashrc is loaded, and that is where corepack and node are added to PATH
    docker run --interactive --rm $tag bash -l -c `
        "mkdir /tsgen && tar --extract --gzip --file - --directory /tsgen && /tsgen/tests/test.ps1" 
}