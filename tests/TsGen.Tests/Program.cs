using System;

namespace TsGen.Tests
{
    sealed class Program
    {
        public static void Main()
        {
            using var webHost = TestWebApi.BuildWebHost();
            webHost.Start();
            Console.WriteLine("<ENTER>");
            Console.ReadLine();
        }
    }
}
