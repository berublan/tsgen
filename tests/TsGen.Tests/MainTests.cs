using System;
using System.IO;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TsGen.Tests
{
    [TestClass]
    public class MainTests
    {
        static TsTypeReader GetReader()
        {
            var config = new WebApiTsTypeReaderConfig(false, true, Util.JsonNetResultTypeMapper);
            return new TsTypeReader(config);
        }

        static TsTypeFactory ReadTestClasses()
        {
            var r = GetReader();

            r.Read(typeof(PersonController));

            var intAlias = new TsAlias(
                r.Factory,
                new TsTypeName(new TsName("int")),
                TsPrimitive.Number
            );
            _ = new TsAlias(
                r.Factory,
                new TsTypeName(new TsName("Alias")),
                new TsObject(
                    new[]
                    {
                        new TsProperty("id", intAlias.Name),
                        new TsProperty(
                            "imposible",
                            new TsIntersection(TsPrimitive.String, TsPrimitive.Void)
                        ),
                        new TsProperty(
                            "desc",
                            new TsUnion(
                                TsPrimitive.String,
                                TsPrimitive.Null,
                                TsPrimitive.Undefined
                            ),
                            optional: true
                        ),
                    }
                )
            );

            return r.Factory;
        }

        private static bool ShouldUpdateSnapshot()
        {
            var x = Environment.GetEnvironmentVariable("UPDATE_SNAPSHOT");
            return x == "1" || "true".Equals(x, StringComparison.OrdinalIgnoreCase);
        }

        void VerifySnapshot(TsTypeFactory factory, TsWriterSettings settings, string outFileName)
        {
            var tsFile = new TsFile(factory.RootNamespace);

            var tsProjPath = Path.GetFullPath(
                Path.Combine(
                    Path.GetDirectoryName(GetType().Assembly.Location)!,
                    "..",
                    "..",
                    "..",
                    "typescript"
                )
            );

            var generatedPath = Path.Combine(tsProjPath, "generated");

            Directory.CreateDirectory(generatedPath);

            string actual;
            using (var w = new StringWriter())
            {
                TsWriter.WriteTo(factory, tsFile, w, settings);
                actual = w.ToString();
            }

            var snapshotFile = Path.Combine(generatedPath, outFileName + ".ts");
            if (ShouldUpdateSnapshot() || !File.Exists(snapshotFile))
            {
                Directory.CreateDirectory(generatedPath);
                File.WriteAllText(snapshotFile, actual);
            }
            else
            {
                var expected = File.ReadAllText(snapshotFile).ReplaceLineEndings();
                actual.Should().Be(expected);
            }
        }

        [TestMethod]
        public void TsAngularJsClientWriterOutput()
        {
            var settings = new TsWriterSettings();
            TsWriter.SetAngularJsSettings(settings);
            settings.ConstEnums = true;
            settings.EnumMode = TsEnumMode.String;
            settings.GenerateClass = true;
            VerifySnapshot(ReadTestClasses(), settings, "AngularJsClient");
        }

        [TestMethod]
        public void TsAngularClientWriterOutput()
        {
            var settings = new TsWriterSettings();
            TsWriter.SetAngularSettings(settings);
            settings.ConstEnums = true;
            settings.EnumMode = TsEnumMode.String;
            VerifySnapshot(ReadTestClasses(), settings, "AngularClient");
        }

        [TestMethod]
        public void TsClientWriterOutput()
        {
            var settings = new TsWriterSettings();
            VerifySnapshot(ReadTestClasses(), settings, "BaseClient");
        }

        [TestMethod]
        public void EndToEnd()
        {
            var r = GetReader();

            r.Read(typeof(TestController));

            var settings = new TsWriterSettings { GenerateClass = true, Readonly = true };
            VerifySnapshot(r.Factory, settings, "TestClient");
        }
    }
}
