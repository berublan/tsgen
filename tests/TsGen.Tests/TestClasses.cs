#nullable disable

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TsGen.Tests
{
    public class BasePerson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public bool? Active { get; set; }

        [CanBeNull]
        public string Hobby { get; set; }
    }

    public class Person : BasePerson
    {
        public DateTimeOffset? Apointment { get; set; }
        public IReadOnlyList<DateTime> FavouriteDays { get; set; }
        public IReadOnlyList<DayOfWeek> FavouriteDaysOfWeek { get; set; }
        public IReadOnlyList<Person> Friends { get; set; }
        public IReadOnlyDictionary<string, Vehicle.Car> CarsByPlate { get; set; }
        public SortedDictionary<string, int> SortedDict { get; set; }
        public Dictionary<string, float> NormalDict { get; set; }
        public Option<Person> BestFriend { get; set; }

        [CanBeNull]
        public Tree<int[]> Something { get; set; }

        [CanBeNull]
        public Vehicle.Car Car { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = false)]
    public class CanBeNullAttribute : Attribute { }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class HttpGetAttribute : Attribute { }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class HttpPostAttribute : Attribute { }

#nullable enable
    public class NullableRef
    {
        public string? Simple { get; set; }
        public Dictionary<int, string?>? NestedGeneric { get; set; }
    }

    public class JsonNetResult<T>
    {
        public T Value { get; set; } = default!;
    }

#nullable disable

#pragma warning disable IDE1006, CA1715
    public interface PersonController
#pragma warning restore
    {
        [HttpGet]
        [CanBeNull]
        Person GetById(int id);

        [HttpGet]
        IReadOnlyList<Person> GetAll();

        [HttpPost]
        void Save(Person person);

        [HttpPost]
        bool Plant(Tree<Vehicle.Car> cs);

        [HttpPost]
        DateTime GetBirthDay(Option<Person> op);

        [HttpPost]
        MapperStackOverFlowBug MapperStackOverFlowBug();

        [HttpGet]
        NonNamespaceClass NonNamespaceClass();

#nullable enable
        [HttpPost]
        NullableRef? NullableRef(NullableRef? @ref);

        [HttpGet]
        Task<IReadOnlyDictionary<string, int>> NullabilityInfoGenericTypeArgumentsIssue();

        [HttpGet]
        JsonNetResult<string?> NullabilityInfoTypeMapperIssue();

        [HttpGet]
        Task<string?> NullabilityInfoTaskIssue();

        [HttpGet]
        Task<IInheritedInterface> GetInheritedInterface();

#nullable disable

        [HttpPost]
        IEnumerable<DateTime> ArrayOfDatesMapping();

        [HttpGet]
        IReadOnlyList<Issue.A> Issue();

        [HttpGet]
        Issue2.A Issue2();

        [HttpGet]
        Issue3.A<int> Issue3();

        [HttpGet]
        Issue4.A Issue4();
    }

    namespace Vehicle
    {
        public class Car
        {
            public string Plate { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public ConsoleColor Color { get; set; }
            public DateTime Registration { get; set; }
            public TimeSpan Runtime { get; set; }
        }
    }

    public class Option<T>
    {
        public bool HasValue { get; set; }
        public T Value { get; set; }
    }

    public class Tree<T>
    {
        public T Value { get; set; }
        public Tree<T> Left { get; set; }
        public Tree<T> Right { get; set; }
    }

    public class MapperStackOverFlowBug
    {
#pragma warning disable IDE1006
        public IReadOnlyList<MapperStackOverFlowBug> xs { get; set; }
#pragma warning restore
    }

    public interface IInheritedInterface : IBaseInterface
    {
        int? Count { get; }
    }

    public interface IBaseInterface
    {
        DateTime From { get; }
        DateTime? To { get; }
    }

    public class InheritedInterfaceImpl : IInheritedInterface
    {
        public int? Count { get; init; }

        public DateTime From { get; init; }

        public DateTime? To { get; init; }
    }

    namespace Issue
    {
        public class A : B<C> { }

        public class B<TD>
            where TD : D
        {
            public IReadOnlyList<E> Es { get; set; }
            public TD D { get; set; }
        }

        public class D : DBase { }

        public class DBase { }

        public class C : D { }

        public class E { }
    }

    namespace Issue2
    {
        public class A : B
        {
            public DateTime D { get; set; }
        }

        public class B
        {
            public string S { get; set; }
        }
    }

    namespace Issue3
    {
        public class A<T> : B<T>
        {
            public T ValueOnA { get; set; }
        }

        public class B<T> : C
        {
            public T ValueOnB { get; set; }
        }

        public class C
        {
            public string S { get; set; }
        }
    }

    namespace Issue4
    {
        public record class A(int Id, string Name);
    }
}

#pragma warning disable CA1050
public class NonNamespaceClass
#pragma warning restore
{
    public int Id { get; set; }
    public string Name { get; set; }
}
