import { TsGen } from "../generated/TestClient.js";
import { createFetchReqFun } from "./fetch.js";
import * as net from "net";
import * as path from "path";
import { spawn } from "child_process";
import assert from "assert";
import { fileURLToPath } from "url";
import { randomUUID } from "crypto";

const port = 5000;
const host = "127.0.0.1";

async function isServerUp(): Promise<boolean> {
    return new Promise<boolean>((res) => {
        const timeoutHandle = setTimeout(() => {
            res(false);
            socket.end();
        }, 500);
        const socket = net.createConnection(port, host, () => {
            clearTimeout(timeoutHandle);
            res(true);
            socket.end();
        });
        socket.on("error", (err) => {
            clearTimeout(timeoutHandle);
            res(false);
        });
    });
}

interface Server {
    close(): void;
}

const __dirname = path.dirname(fileURLToPath(import.meta.url));

async function startServer(): Promise<Server> {
    const projDir = __dirname.replace(/(TsGen\.Tests).*/, "$1");
    const projPath = path.resolve(projDir, "TsGen.Tests.csproj");
    return new Promise<Server>((res, rej) => {
        const proc = spawn("dotnet", ["run", "--project", projPath]);
        proc.on("error", rej);
        proc.stdout.pipe(process.stdout);
        const server: Server = {
            close() {
                proc.stdin.write("\n");
            },
        };
        proc.stdout.on("data", (data) => {
            const str = data.toString();
            if (/<ENTER>/.test(str)) {
                res(server);
            }
        });
    });
}

async function runTest(client: TsGen.Tests.TestController) {
    {
        const expected = await client.CreateExpected();
        // TODO better way to check that all dates were mapped
        assert(expected.data.birthDate instanceof Date);
        for (const plate of Object.keys(expected.data.carsByPlate)) {
            const car = expected.data.carsByPlate[plate];
            assert(car.registration instanceof Date);
        }
        await client.SetActual(
            expected.data,
            expected.id,
            expected.guid,
            expected.name,
            expected.date,
        );
        const actual = await client.GetActual();
        assert.deepStrictEqual(actual, expected);
        await client.Verify();
    }

    {
        const actual = await client.Passthrough(null);
        console.log(actual);
        assert.deepStrictEqual(actual, null);
    }

    {
        const expected: TsGen.Tests.TestRequest<string | null> = {
            data: null,
            date: new Date(),
            id: 199,
            name: "foobarbaz",
            guid: randomUUID(),
        };
        const actual = await client.Passthrough(expected);
        assert.deepStrictEqual(actual, expected);
    }
}

async function main() {
    let server: Server | undefined;
    try {
        const isUp = await isServerUp();
        if (!isUp) {
            console.log("Starting server");
            server = await startServer();
        } else {
            console.log("Server was up");
        }

        const requestFunction = createFetchReqFun(
            `http://${host}:${port}/Test`,
        );

        await runTest(TsGen.Tests.TestControllerFactory(requestFunction));
        await runTest(new TsGen.Tests.TestController(requestFunction));
    } catch (e) {
        console.error(e);
        process.exitCode = 1;
    } finally {
        if (server) {
            server.close();
        }
    }
}

main();
