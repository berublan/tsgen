export type RequestFunction<P extends PromiseLike<any>> = (
    method: string,
    actionName: string,
    queryParams: QueryParams,
    body?: Object | null,
) => P;

export interface QueryParams {
    [name: string]: any;
}
