import type { QueryParams, RequestFunction } from "./types";

export const toQueryString = (q: QueryParams) => {
    if (!q) {
        return "";
    }
    let sb = "";
    let delim = "?";
    for (const k of Object.getOwnPropertyNames(q)) {
        sb += delim;
        delim = "&";
        const value = q[k];
        let valueStr: string;
        if (typeof value === "number") {
            valueStr = value.toString();
        } else if (typeof value === "string") {
            valueStr = value;
        } else if (value === undefined || value === null) {
            valueStr = "";
        } else if ("toJSON" in value && typeof value.toJSON === "function") {
            valueStr = value.toJSON();
        } else {
            throw new Error(`Not supported on query string: ${value}`);
        }
        sb += `${encodeURIComponent(k)}=${encodeURIComponent(valueStr)}`;
    }
    return sb;
};

const defaultHeaders = {
    "content-type": "application/json",
};

function isEmpty(r: Response) {
    const cl =
        r.headers.get("content-length") || r.headers.get("Content-Length");
    return cl === "0";
}

export function createFetchReqFun(
    baseUrl: string,
): RequestFunction<Promise<any>> {
    baseUrl = baseUrl.replace(/\/$/, "");
    return (
        method: string,
        actionName: string,
        queryParams: QueryParams,
        body?: Object | null,
    ) => {
        let bodyStr;
        if (body) {
            bodyStr = JSON.stringify(body);
        }
        const url = baseUrl + "/" + actionName + toQueryString(queryParams);
        console.log("Url", url);
        return fetch(url, {
            headers: defaultHeaders,
            method: method,
            body: bodyStr,
        }).then((r) => {
            if (r.status !== 200) {
                return r.text().then((txt) =>
                    Promise.reject({
                        status: r.status,
                        statusText: r.statusText,
                        text: txt,
                    }),
                );
            }
            if (isEmpty(r)) {
                return void 0;
            }
            return r.json();
        });
    };
}
