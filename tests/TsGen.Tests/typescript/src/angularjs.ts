import type * as ng from "angular";
import type { RequestFunction, QueryParams } from "./types";

export function createReqFun(
    baseUrl: string,
    $http: ng.IHttpService,
): RequestFunction<ng.IPromise<any>> {
    baseUrl = baseUrl.replace(/\/$/, "");
    return (
        method: string,
        actionName: string,
        queryParams: QueryParams,
        body?: Object | null,
    ) => {
        return $http({
            url: baseUrl + "/" + actionName,
            method: method,
            params: queryParams,
            data: body,
        }).then((r) => r.data);
    };
}
