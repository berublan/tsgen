/* eslint-disable */
// tslint:disable
// tool generated
export type RequestFunction = (method: string, actionName: string, queryParams: QueryParams, body?: Object | null) => Promise<any>;
export interface QueryParams {
	[name: string]: any;
}

export interface NonNamespaceClass {
	id: number;
	name: string;
}

export type int = number;

export type Alias = { id: int, imposible: string & void, desc?: string | null | undefined };

export namespace TsGen {
	
	export namespace Tests {
		
		export interface BasePerson {
			id: number;
			name: string;
			birthDate: Date;
			active: boolean | null;
			hobby: string | null;
		}
		
		export interface Person extends BasePerson {
			apointment: Date | null;
			favouriteDays: Array<Date>;
			favouriteDaysOfWeek: Array<System.DayOfWeek>;
			friends: Array<Person>;
			carsByPlate: { [key: string]: Vehicle.Car };
			sortedDict: { [key: string]: number };
			normalDict: { [key: string]: number };
			bestFriend: Option<Person>;
			something: Tree<Array<number>> | null;
			car: Vehicle.Car | null;
		}
		
		export interface Option<T> {
			hasValue: boolean;
			value: T;
		}
		
		export interface Tree<T> {
			value: T;
			left: Tree<T>;
			right: Tree<T>;
		}
		
		export interface MapperStackOverFlowBug {
			xs: Array<MapperStackOverFlowBug>;
		}
		
		export interface NullableRef {
			simple: string | null;
			nestedGeneric: { [key: number]: string | null } | null;
		}
		
		export interface IBaseInterface {
			from: Date;
			to: Date | null;
		}
		
		export interface IInheritedInterface extends IBaseInterface {
			count: number | null;
		}
		
		export interface PersonController {
			GetById: (id: number) => Promise<Person | null>,
			GetAll: () => Promise<Array<Person>>,
			Save: (person: Person) => Promise<void>,
			Plant: (cs: Tree<Vehicle.Car>) => Promise<boolean>,
			GetBirthDay: (op: Option<Person>) => Promise<Date>,
			MapperStackOverFlowBug: () => Promise<MapperStackOverFlowBug>,
			NonNamespaceClass: () => Promise<NonNamespaceClass>,
			NullableRef: (ref: NullableRef | null) => Promise<NullableRef | null>,
			NullabilityInfoGenericTypeArgumentsIssue: () => Promise<{ [key: string]: number }>,
			NullabilityInfoTypeMapperIssue: () => Promise<string | null>,
			NullabilityInfoTaskIssue: () => Promise<string | null>,
			GetInheritedInterface: () => Promise<IInheritedInterface>,
			ArrayOfDatesMapping: () => Promise<Array<Date>>,
			Issue: () => Promise<Array<Issue.A>>,
			Issue2: () => Promise<Issue2.A>,
			Issue3: () => Promise<Issue3.A<number>>,
			Issue4: () => Promise<Issue4.A>,
		}
		
		export function PersonControllerFactory(requestFunction: RequestFunction): PersonController {
			return {
				GetById: (id) => 
					requestFunction("GET", "GetById", { id }).then((x: Person) => x && decodeTsGenTestsPerson(x)),
				GetAll: () => 
					requestFunction("GET", "GetAll", {}),
				Save: (person) => 
					requestFunction("POST", "Save", {}, person),
				Plant: (cs) => 
					requestFunction("POST", "Plant", {}, cs),
				GetBirthDay: (op) => 
					requestFunction("POST", "GetBirthDay", {}, op).then((x: Date) => x && new Date(x)),
				MapperStackOverFlowBug: () => 
					requestFunction("POST", "MapperStackOverFlowBug", {}),
				NonNamespaceClass: () => 
					requestFunction("GET", "NonNamespaceClass", {}),
				NullableRef: (ref) => 
					requestFunction("POST", "NullableRef", {}, ref),
				NullabilityInfoGenericTypeArgumentsIssue: () => 
					requestFunction("GET", "NullabilityInfoGenericTypeArgumentsIssue", {}),
				NullabilityInfoTypeMapperIssue: () => 
					requestFunction("GET", "NullabilityInfoTypeMapperIssue", {}),
				NullabilityInfoTaskIssue: () => 
					requestFunction("GET", "NullabilityInfoTaskIssue", {}),
				GetInheritedInterface: () => 
					requestFunction("GET", "GetInheritedInterface", {}).then(decodeTsGenTestsIInheritedInterface),
				ArrayOfDatesMapping: () => 
					requestFunction("POST", "ArrayOfDatesMapping", {}).then((x: Array<Date>) => x && x.map((x: Date) => x && new Date(x))),
				Issue: () => 
					requestFunction("GET", "Issue", {}),
				Issue2: () => 
					requestFunction("GET", "Issue2", {}).then(decodeTsGenTestsIssue2A),
				Issue3: () => 
					requestFunction("GET", "Issue3", {}),
				Issue4: () => 
					requestFunction("GET", "Issue4", {}),
			};
		}
		
		export namespace Vehicle {
			
			export interface Car {
				plate: string;
				make: string;
				model: string;
				color: System.ConsoleColor;
				registration: Date;
				runtime: System.TimeSpan;
			}
			
		}
		
		export namespace Issue {
			
			export interface B<TD> {
				es: Array<E>;
				d: TD;
			}
			
			export interface E {
			}
			
			export interface DBase {
			}
			
			export interface D extends DBase {
			}
			
			export interface C extends D {
			}
			
			export interface A extends B<C> {
			}
			
		}
		
		export namespace Issue2 {
			
			export interface B {
				s: string;
			}
			
			export interface A extends B {
				d: Date;
			}
			
		}
		
		export namespace Issue3 {
			
			export interface C {
				s: string;
			}
			
			export interface B<T> extends C {
				valueOnB: T;
			}
			
			export interface A<T> extends B<T> {
				valueOnA: T;
			}
			
		}
		
		export namespace Issue4 {
			
			export interface A {
				id: number;
				name: string;
			}
			
		}
		
	}
	
}

export namespace System {
	
	export enum DayOfWeek {
		Sunday = 0,
		Monday = 1,
		Tuesday = 2,
		Wednesday = 3,
		Thursday = 4,
		Friday = 5,
		Saturday = 6,
	}
	
	export enum ConsoleColor {
		Black = 0,
		DarkBlue = 1,
		DarkGreen = 2,
		DarkCyan = 3,
		DarkRed = 4,
		DarkMagenta = 5,
		DarkYellow = 6,
		Gray = 7,
		DarkGray = 8,
		Blue = 9,
		Green = 10,
		Cyan = 11,
		Red = 12,
		Magenta = 13,
		Yellow = 14,
		White = 15,
	}
	
	export type TimeSpan = string;
	
}


function decodeTsGenTestsPerson(x: TsGen.Tests.Person): TsGen.Tests.Person {
	return {
		apointment: x.apointment && new Date(x.apointment),
		favouriteDays: x.favouriteDays && x.favouriteDays.map((x: Date) => x && new Date(x)),
		favouriteDaysOfWeek: x.favouriteDaysOfWeek,
		friends: x.friends,
		carsByPlate: x.carsByPlate && objectMap(x.carsByPlate, decodeTsGenTestsVehicleCar),
		sortedDict: x.sortedDict,
		normalDict: x.normalDict,
		bestFriend: x.bestFriend,
		something: x.something,
		car: x.car && decodeTsGenTestsVehicleCar(x.car),
		id: x.id,
		name: x.name,
		birthDate: x.birthDate && new Date(x.birthDate),
		active: x.active,
		hobby: x.hobby,
	};
}
function decodeTsGenTestsVehicleCar(x: TsGen.Tests.Vehicle.Car): TsGen.Tests.Vehicle.Car {
	return {
		plate: x.plate,
		make: x.make,
		model: x.model,
		color: x.color,
		registration: x.registration && new Date(x.registration),
		runtime: x.runtime,
	};
}
function decodeTsGenTestsIInheritedInterface(x: TsGen.Tests.IInheritedInterface): TsGen.Tests.IInheritedInterface {
	return {
		count: x.count,
		from: x.from && new Date(x.from),
		to: x.to && new Date(x.to),
	};
}
function decodeTsGenTestsIssue2A(x: TsGen.Tests.Issue2.A): TsGen.Tests.Issue2.A {
	return {
		d: x.d && new Date(x.d),
		s: x.s,
	};
}
function objectMap<T, R>(obj: Record<string, T>, map: (x: T) => R): Record<string, R> {
	const res: Record<string, R> = {};
	for (const key of Object.keys(obj)) {
		res[key] = map(obj[key]);
	}
	return res;
}