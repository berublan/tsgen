/* eslint-disable */
// tslint:disable
// tool generated
export type RequestFunction = (method: string, actionName: string, queryParams: QueryParams, body?: Object | null) => Promise<any>;
export interface QueryParams {
	[name: string]: any;
}

export namespace TsGen {
	
	export namespace Tests {
		
		export interface TestRequest<T> {
			readonly id: number;
			readonly guid: System.Guid;
			readonly name: string;
			readonly date: Date;
			readonly data: T;
		}
		
		export interface BasePerson {
			readonly id: number;
			readonly name: string;
			readonly birthDate: Date;
			readonly active: boolean | null;
			readonly hobby: string | null;
		}
		
		export interface Person extends BasePerson {
			readonly apointment: Date | null;
			readonly favouriteDays: ReadonlyArray<Date>;
			readonly favouriteDaysOfWeek: ReadonlyArray<System.DayOfWeek>;
			readonly friends: ReadonlyArray<Person>;
			readonly carsByPlate: { readonly [key: string]: Vehicle.Car };
			readonly sortedDict: { readonly [key: string]: number };
			readonly normalDict: { readonly [key: string]: number };
			readonly bestFriend: Option<Person>;
			readonly something: Tree<ReadonlyArray<number>> | null;
			readonly car: Vehicle.Car | null;
		}
		
		export interface Option<T> {
			readonly hasValue: boolean;
			readonly value: T;
		}
		
		export interface Tree<T> {
			readonly value: T;
			readonly left: Tree<T>;
			readonly right: Tree<T>;
		}
		
		export interface TestController {
			CreateExpected: () => Promise<TestRequest<Person>>,
			GetExpected: () => Promise<TestRequest<Person>>,
			SetActual: (person: Person, id: number, guid: System.Guid, name: string, date: Date) => Promise<void>,
			GetActual: () => Promise<TestRequest<Person>>,
			Verify: () => Promise<void>,
			Passthrough: (x: TestRequest<string | null> | null) => Promise<TestRequest<string | null> | null>,
		}
		
		export function TestControllerFactory(requestFunction: RequestFunction): TestController {
			return {
				CreateExpected: () => 
					requestFunction("POST", "CreateExpected", {}).then(decodeTsGenTestsTestRequestOfTsGenTestsPerson),
				GetExpected: () => 
					requestFunction("POST", "GetExpected", {}).then(decodeTsGenTestsTestRequestOfTsGenTestsPerson),
				SetActual: (person, id, guid, name, date) => 
					requestFunction("POST", "SetActual", { id, guid, name, date }, person),
				GetActual: () => 
					requestFunction("GET", "GetActual", {}).then(decodeTsGenTestsTestRequestOfTsGenTestsPerson),
				Verify: () => 
					requestFunction("GET", "Verify", {}),
				Passthrough: (x) => 
					requestFunction("POST", "Passthrough", {}, x).then((x: TestRequest<string>) => x && decodeTsGenTestsTestRequestOfstring(x)),
			};
		}
		
		export const TestController = class {
			constructor(requestFunction: RequestFunction) {
				Object.assign(this, TestControllerFactory(requestFunction));
			}
		} as unknown as new (requestFunction: RequestFunction) => TestController
		
		export namespace Vehicle {
			
			export interface Car {
				readonly plate: string;
				readonly make: string;
				readonly model: string;
				readonly color: System.ConsoleColor;
				readonly registration: Date;
				readonly runtime: System.TimeSpan;
			}
			
		}
		
	}
	
}

export namespace System {
	
	export type Guid = string;
	
	export enum DayOfWeek {
		Sunday = 0,
		Monday = 1,
		Tuesday = 2,
		Wednesday = 3,
		Thursday = 4,
		Friday = 5,
		Saturday = 6,
	}
	
	export enum ConsoleColor {
		Black = 0,
		DarkBlue = 1,
		DarkGreen = 2,
		DarkCyan = 3,
		DarkRed = 4,
		DarkMagenta = 5,
		DarkYellow = 6,
		Gray = 7,
		DarkGray = 8,
		Blue = 9,
		Green = 10,
		Cyan = 11,
		Red = 12,
		Magenta = 13,
		Yellow = 14,
		White = 15,
	}
	
	export type TimeSpan = string;
	
}


function decodeTsGenTestsTestRequestOfTsGenTestsPerson(x: TsGen.Tests.TestRequest<TsGen.Tests.Person>): TsGen.Tests.TestRequest<TsGen.Tests.Person> {
	return {
		id: x.id,
		guid: x.guid,
		name: x.name,
		date: x.date && new Date(x.date),
		data: x.data && decodeTsGenTestsPerson(x.data),
	};
}
function decodeTsGenTestsPerson(x: TsGen.Tests.Person): TsGen.Tests.Person {
	return {
		apointment: x.apointment && new Date(x.apointment),
		favouriteDays: x.favouriteDays && x.favouriteDays.map((x: Date) => x && new Date(x)),
		favouriteDaysOfWeek: x.favouriteDaysOfWeek,
		friends: x.friends,
		carsByPlate: x.carsByPlate && objectMap(x.carsByPlate, decodeTsGenTestsVehicleCar),
		sortedDict: x.sortedDict,
		normalDict: x.normalDict,
		bestFriend: x.bestFriend,
		something: x.something,
		car: x.car && decodeTsGenTestsVehicleCar(x.car),
		id: x.id,
		name: x.name,
		birthDate: x.birthDate && new Date(x.birthDate),
		active: x.active,
		hobby: x.hobby,
	};
}
function decodeTsGenTestsVehicleCar(x: TsGen.Tests.Vehicle.Car): TsGen.Tests.Vehicle.Car {
	return {
		plate: x.plate,
		make: x.make,
		model: x.model,
		color: x.color,
		registration: x.registration && new Date(x.registration),
		runtime: x.runtime,
	};
}
function decodeTsGenTestsTestRequestOfstring(x: TsGen.Tests.TestRequest<string>): TsGen.Tests.TestRequest<string> {
	return {
		id: x.id,
		guid: x.guid,
		name: x.name,
		date: x.date && new Date(x.date),
		data: x.data,
	};
}
function objectMap<T, R>(obj: Record<string, T>, map: (x: T) => R): Record<string, R> {
	const res: Record<string, R> = {};
	for (const key of Object.keys(obj)) {
		res[key] = map(obj[key]);
	}
	return res;
}