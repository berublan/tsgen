using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.DependencyInjection;

namespace TsGen.Tests
{
    public static class TestWebApi
    {
        public static IWebHost BuildWebHost()
        {
            var webHost = WebHost
                .CreateDefaultBuilder()
                .Configure(app =>
                {
                    app.UseMvc(router =>
                    {
                        router.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
                    });
                })
                .ConfigureServices(services =>
                {
                    services.AddLogging();
                    services.AddMvc(options =>
                    {
                        options.EnableEndpointRouting = false;
                        options.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
                    });
                })
                .UseKestrel()
                .ConfigureKestrel(options =>
                {
                    options.Listen(IPAddress.Loopback, 5000);
                })
                .Build();
            return webHost;
        }
    }

#nullable disable
    public class TestRequest<T>
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public T Data { get; set; }
    }

#nullable enable

    public class TestController : Controller
    {
        readonly Fixture fixture;

        public TestController()
        {
            fixture = new Fixture();

            fixture
                .Behaviors.OfType<ThrowingRecursionBehavior>()
                .ToList()
                .ForEach(b => fixture.Behaviors.Remove(b));
            fixture.Behaviors.Add(new OmitOnRecursionBehavior(2));
        }

#nullable disable
        private static TestRequest<Person> expected;
        private static TestRequest<Person> actual;

#nullable enable

        [HttpPost]
        public Task<TestRequest<Person>> CreateExpected()
        {
            expected = fixture.Create<TestRequest<Person>>();
            return Task.FromResult(expected);
        }

        [HttpPost]
        public TestRequest<Person> GetExpected()
        {
            return expected;
        }

        [HttpPost]
        public void SetActual(
            [FromBody] Person person,
            [FromQuery] int id,
            [FromQuery] Guid guid,
            [FromQuery] string name,
            [FromQuery] DateTime date
        )
        {
            actual = new TestRequest<Person>
            {
                Id = id,
                Guid = guid,
                Name = name,
                Date = date,
                Data = person,
            };
        }

        [HttpGet]
        public TestRequest<Person> GetActual()
        {
            return actual;
        }

        [HttpGet]
        public Task Verify()
        {
            actual
                .Should()
                .BeEquivalentTo(
                    expected,
                    opt =>
                        opt.Using<DateTime>(ctx =>
                                ctx.Subject.ToUniversalTime()
                                    .Should()
                                    .BeCloseTo(
                                        ctx.Expectation.ToUniversalTime(),
                                        TimeSpan.FromMilliseconds(1)
                                    )
                            )
                            .WhenTypeIs<DateTime>()
                            .Using<DateTimeOffset>(ctx =>
                                ctx.Subject.UtcDateTime.Should()
                                    .BeCloseTo(
                                        ctx.Expectation.UtcDateTime,
                                        TimeSpan.FromMilliseconds(1)
                                    )
                            )
                            .WhenTypeIs<DateTimeOffset>()
                );

            Term.PrintGreenBox("Verify Succeded");
            return Task.CompletedTask;
        }

        [HttpPost]
        public TestRequest<string?>? Passthrough([FromBody] TestRequest<string?>? x)
        {
            return x;
        }
    }

    static class Term
    {
        public const string Black = "\u001b[30m";
        public const string BgGreen = "\u001b[42m";
        public const string Reset = "\u001b[0m";

        public static void PrintGreenBox(string txt)
        {
            const int marginCount = 2;

            var margin = new string(' ', marginCount);
            var emptyLine = new string(' ', marginCount * 2 + txt.Length);

            Console.Write(Black);
            Console.Write(BgGreen);
            Console.Write(emptyLine);
            Console.Write(Reset);
            Console.WriteLine();

            Console.Write(Black);
            Console.Write(BgGreen);
            Console.Write(margin);
            Console.Write(txt);
            Console.Write(margin);
            Console.Write(Reset);
            Console.WriteLine();

            Console.Write(Black);
            Console.Write(BgGreen);
            Console.Write(emptyLine);
            Console.Write(Reset);
            Console.WriteLine();
        }
    }
}
