using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TsGen
{
    public class DecodeMethodWriter
    {
        readonly record struct DecodingInfo(
            bool NeedsDecoding,
            ITsType NormalizedType,
            string? DecodeMethodName
        );

        // null value means we are currently calculating it
        readonly Dictionary<ITsType, DecodingInfo> _decodingInfos = new();
        bool _writeIndexableDecodeMethod = false;
        readonly TsTypeFactory _typeFactory;

        public DecodeMethodWriter(TsTypeFactory typeFactory)
        {
            _typeFactory = typeFactory;
        }

        static ITsType Normalize(ITsType tsType)
        {
            if (tsType is TsUnion tu)
            {
                var nonNullOrUndefTypes = tu
                    .Types.Select(Normalize)
                    .Where(t => !(t.Equals(TsPrimitive.Null) || t.Equals(TsPrimitive.Undefined)))
                    .ToList();
                if (nonNullOrUndefTypes.Count == 1)
                {
                    return nonNullOrUndefTypes[0];
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
            else if (tsType is TsTypeName tn)
            {
                if (tsType is TsGenericParameterTypeName)
                {
                    return tsType;
                }
                else
                {
                    return new TsTypeName(
                        tn.Name,
                        tn.GenericTypeParameters.Select(Normalize).ToList()
                    );
                }
            }
            else if (tsType is TsIndexable ti)
            {
                return new TsIndexable(Normalize(ti.KeyType), Normalize(ti.ValueType));
            }
            else
            {
                return tsType;
            }
        }

        DecodingInfo GetOrCreateDecodingDetail(ITsType tsType)
        {
            var normalizedType = Normalize(tsType);
            if (_decodingInfos.TryGetValue(normalizedType, out var decodingInfo))
            {
                return decodingInfo;
            }

            // placeholder to avoid infinite recursion
            decodingInfo = new DecodingInfo(false, normalizedType, null);
            _decodingInfos.Add(normalizedType, decodingInfo);

            decodingInfo = CreateDecodingInfo(normalizedType);

            _decodingInfos[normalizedType] = decodingInfo;

            return decodingInfo;
        }

        DecodingInfo CreateDecodingInfo(ITsType normalizedType)
        {
            if (normalizedType is TsPrimitive tp)
            {
                var needsDecoding = tp.Equals(TsPrimitive.Date);
                return new DecodingInfo(needsDecoding, normalizedType, null);
            }
            else if (normalizedType is TsIndexable ti)
            {
                var needsDecoding = GetOrCreateDecodingDetail(ti.ValueType).NeedsDecoding;
                return new DecodingInfo(needsDecoding, normalizedType, null);
            }
            else if (normalizedType is TsTypeName tn)
            {
                if (tn is TsGenericParameterTypeName)
                {
                    return new DecodingInfo(false, normalizedType, null);
                }
                else if (tn.GetArrayElementType() is ITsType elementType)
                {
                    var needsDecoding = GetOrCreateDecodingDetail(elementType).NeedsDecoding;
                    return new DecodingInfo(needsDecoding, normalizedType, null);
                }
                else
                {
                    var tIface = _typeFactory.GetOrInstantiateInterface(tn);
                    if (tIface == null)
                    {
                        // is an enum, a generic type parameter or a non instantianted interface
                        return new DecodingInfo(false, normalizedType, null);
                    }
                    var needsDecoding = false;
                    // call GetOrCreateDecodingDetail on all properties (including inherited ones)
                    // to ensure we have all de types we need in _decodingInfos
                    // for WriteDecodeMethods
                    foreach (var p in GetAllProps(tIface))
                    {
                        if (GetOrCreateDecodingDetail(p.Type).NeedsDecoding)
                        {
                            needsDecoding = true;
                        }
                    }

                    string? decodeMethodName;
                    if (needsDecoding)
                    {
                        decodeMethodName = DecodeMethodName(tn);
                    }
                    else
                    {
                        decodeMethodName = null;
                    }
                    return new DecodingInfo(needsDecoding, normalizedType, decodeMethodName);
                }
            }

            throw new NotSupportedException();
        }

        // includes inherited props
        IEnumerable<TsProperty> GetAllProps(TsInterface tsIface)
        {
            foreach (var tsProp in tsIface.Props)
            {
                yield return tsProp;
            }
            foreach (var inheritedTsTypeName in tsIface.Inherits)
            {
                var inheritedTsIface = _typeFactory.GetOrInstantiateInterface(inheritedTsTypeName);
                if (inheritedTsIface != null)
                {
                    foreach (var inheritedTsProp in GetAllProps(inheritedTsIface))
                    {
                        yield return inheritedTsProp;
                    }
                }
            }
        }

        static string DecodeMethodName(TsTypeName tn)
        {
            static void Step(StringWriter w, ITsType tsType)
            {
                if (tsType is TsTypeName tn)
                {
                    foreach (var x in tn.Name.Namespc)
                    {
                        w.Write(x);
                    }
                    w.Write(tn.Name.Name);
                    if (tn.GenericTypeParameters.Count > 0)
                    {
                        w.Write("Of");
                        foreach (var x in tn.GenericTypeParameters)
                        {
                            Step(w, x);
                        }
                    }
                }
                else if (tsType is TsPrimitive tp)
                {
                    w.Write(tp.Name);
                }
                else if (tsType is TsIndexable ti)
                {
                    Step(w, ti.KeyType);
                    w.Write("To");
                    Step(w, ti.ValueType);
                }
                else
                {
                    throw new NotSupportedException(tsType.GetType().ToString());
                }
            }

            var w = new StringWriter();
            w.Write("decode");
            Step(w, tn);
            return w.ToString();
        }

        public bool NeedsDecoding(ITsType tsType)
        {
            return GetOrCreateDecodingDetail(tsType).NeedsDecoding;
        }

        public void WriteDecodeMethodNameOrLambda(TsBaseWriter w, ITsType tsType)
        {
            var decodingInfo = GetOrCreateDecodingDetail(tsType);
            if (!decodingInfo.NeedsDecoding)
            {
                throw new InvalidOperationException(
                    $"Should only call `{nameof(WriteDecodeMethodNameOrLambda)}` if `{nameof(NeedsDecoding)}` returns `true`"
                );
            }

            if (decodingInfo.DecodeMethodName != null)
            {
                if (tsType.Equals(decodingInfo.NormalizedType))
                {
                    w.Write(decodingInfo.DecodeMethodName);
                }
                else
                {
                    w.Write("(x: ");
                    w.WriteTsType(decodingInfo.NormalizedType);
                    w.Write(") => x && ");
                    w.Write(decodingInfo.DecodeMethodName);
                    w.Write("(x)");
                }
            }
            else
            {
                w.Write("(x: ");
                w.WriteTsType(decodingInfo.NormalizedType);
                w.Write(") => ");
                WriteDecodeMethodExpression(w, "x", tsType);
            }
        }

        void WriteDecodeMethodExpression(TsBaseWriter w, string name, ITsType tsType)
        {
            var decodingInfo = GetOrCreateDecodingDetail(tsType);
            if (!decodingInfo.NeedsDecoding)
            {
                throw new InvalidOperationException(
                    $"Should only call `{nameof(WriteDecodeMethodNameOrLambda)}` if `{nameof(NeedsDecoding)}` returns `true`"
                );
            }

            if (decodingInfo.DecodeMethodName != null)
            {
                w.Write($"{name} && ");
                w.Write(decodingInfo.DecodeMethodName);
                w.Write($"({name})");
            }
            else if (
                decodingInfo.NormalizedType is TsTypeName ta
                && ta.GetArrayElementType() is ITsType elemTy
            )
            {
                w.Write($"{name} && {name}.map(");
                WriteDecodeMethodNameOrLambda(w, elemTy);
                w.Write(")");
            }
            else if (decodingInfo.NormalizedType is TsIndexable ti)
            {
                w.Write($"{name} && objectMap({name}, ");
                WriteDecodeMethodNameOrLambda(w, ti.ValueType);
                _writeIndexableDecodeMethod = true;
                w.Write(")");
            }
            else if (decodingInfo.NormalizedType.Equals(TsPrimitive.Date))
            {
                w.Write($"{name} && new Date({name})");
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        static void WriteIndexableDecodeMethod(IndentendWriter w)
        {
            w.Write(
                "function objectMap<T, R>(obj: Record<string, T>, map: (x: T) => R): Record<string, R> {"
            );
            w.IncIndent();
            w.WriteLine();
            w.Write("const res: Record<string, R> = {};");
            w.WriteLine();
            w.Write("for (const key of Object.keys(obj)) {");
            w.IncIndent();
            w.WriteLine();
            w.Write("res[key] = map(obj[key]);");
            w.DecIndent();
            w.WriteLine();
            w.Write("}");
            w.WriteLine();
            w.Write("return res;");
            w.DecIndent();
            w.WriteLine();
            w.Write("}");
        }

        public void WriteDecodeMethods(TsBaseWriter w)
        {
            foreach (var decodingInfo in _decodingInfos.Values)
            {
                if (decodingInfo.DecodeMethodName == null)
                {
                    continue;
                }
                var typeName = (TsTypeName)decodingInfo.NormalizedType;
                w.WriteLine();
                w.Write($"function {decodingInfo.DecodeMethodName}(x: ");
                w.WriteTsTypeName(typeName);
                w.Write("): ");
                w.WriteTsTypeName(typeName);
                w.Write(" {");
                w.IncIndent();
                w.WriteLine();
                w.Write("return {");
                w.IncIndent();
                var tsIface = _typeFactory.GetOrInstantiateInterface(typeName)!;
                foreach (var p in GetAllProps(tsIface))
                {
                    w.WriteLine();
                    if (NeedsDecoding(p.Type))
                    {
                        w.Write($"{p.Name}: ");
                        WriteDecodeMethodExpression(w, $"x.{p.Name}", p.Type);
                        w.Write(",");
                    }
                    else
                    {
                        w.Write($"{p.Name}: x.{p.Name},");
                    }
                }
                w.DecIndent();
                w.WriteLine();
                w.Write("};");
                w.DecIndent();
                w.WriteLine();
                w.Write("}");
            }

            if (_writeIndexableDecodeMethod)
            {
                w.WriteLine();
                WriteIndexableDecodeMethod(w);
            }
        }
    }
}
