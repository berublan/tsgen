using System;
using System.IO;

namespace TsGen
{
    public class IndentendWriter : IDisposable
    {
        readonly bool _ownsWriter;
        readonly TextWriter _w;

        public IndentendWriter(TextWriter w, bool ownsWriter, int margin = 0, string indent = "\t")
        {
            _w = w;
            _ownsWriter = ownsWriter;
            Indent = indent;
            Margin = margin;
        }

        public int Margin { get; private set; }

        public string Indent { get; }

        public void IncIndent()
        {
            Margin++;
        }

        public void DecIndent()
        {
            Margin--;
        }

        public void Write(char x)
        {
            _w.Write(x);
        }

        public void Write(string x)
        {
            _w.Write(x);
        }

        public void WriteLine()
        {
            _w.WriteLine();
            for (var i = 0; i < Margin; i++)
            {
                _w.Write(Indent);
            }
        }

        public void WriteLine(string x)
        {
            WriteLine();
            Write(x);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            if (_ownsWriter)
            {
                _w.Dispose();
            }
        }

        sealed class Indentation : IDisposable
        {
            readonly IndentendWriter _writer;

            public Indentation(IndentendWriter w)
            {
                _writer = w;
                _writer.IncIndent();
            }

            public void Dispose()
            {
                _writer.DecIndent();
            }
        }

        protected IDisposable Indented()
        {
            return new Indentation(this);
        }
    }
}
