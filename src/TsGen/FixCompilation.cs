#if NETFRAMEWORK
namespace System.Reflection
{
    public sealed class NullabilityInfoContext
    {
        public NullabilityInfo? Create(PropertyInfo _)
        {
            return null;
        }

        public NullabilityInfo? Create(ParameterInfo _)
        {
            return null;
        }
    }

    public sealed class NullabilityInfo
    {
#nullable disable
        public NullabilityInfo[] GenericTypeArguments { get; }
        public NullabilityInfo ElementType { get; }
#nullable enable
        public NullabilityState ReadState { get; }
    }

    public enum NullabilityState
    {
        Nullable,
    }
}

namespace System.Runtime.CompilerServices
{
    internal static class IsExternalInit { }
}
#endif
