using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TsGen
{
    public static class Util
    {
        public static (Type, NullabilityInfo?) JsonNetResultTypeMapper(
            Type type,
            NullabilityInfo? nullabilityInfo
        )
        {
            if (
                type.IsGenericType
                && type.GenericTypeArguments.Length == 1
                && type.Name.StartsWith("JsonNetResult`", StringComparison.OrdinalIgnoreCase)
            )
            {
                return (
                    type.GenericTypeArguments[0],
                    nullabilityInfo?.GenericTypeArguments.ElementAtOrDefault(0)
                );
            }
            else
            {
                return (type, nullabilityInfo);
            }
        }

        public static string LowerCamelCasePropertyName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return name;
            }

            if (char.IsLower(name[0]))
            {
                return name;
            }

            var cs = name.ToCharArray();
            cs[0] = char.ToLowerInvariant(cs[0]);
            return new string(cs);
        }

        public static string IndentText(string text, string indent)
        {
            var lines = text.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            var newLines = lines.Select(x => indent + x);
            return string.Join(Environment.NewLine, newLines);
        }

        public static bool AnyBaseType(Type type, Func<Type, bool> predicate)
        {
            if (type == null)
            {
#pragma warning disable CA1510 // has to build on .NET Framework
                throw new ArgumentNullException(nameof(type));
#pragma warning restore CA1510
            }

            while (true)
            {
                if (predicate(type))
                {
                    return true;
                }

                if (type.BaseType == null)
                {
                    return false;
                }
                type = type.BaseType;
            }
        }

        internal static List<R> Map<T, R>(this IReadOnlyList<T> xs, Func<T, R> func)
        {
            var res = new List<R>(xs.Count);
            foreach (var x in xs)
            {
                res.Add(func(x));
            }
            return res;
        }

        internal static SequenceEquatable<T> SequenceEquatable<T>(this IEnumerable<T> xs)
        {
            return new SequenceEquatable<T>(xs);
        }
    }

#pragma warning disable IDE0250, IDE0251  // has to build on .NET Framework
    internal struct SequenceEquatable<T>
    {
        public SequenceEquatable(IEnumerable<T> xs)
        {
            Xs = xs;
        }

        public IEnumerable<T> Xs { get; }

        public override bool Equals(object? obj)
        {
            return obj is SequenceEquatable<T> equatable
                && Enumerable.SequenceEqual(Xs, equatable.Xs);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = -448830564;
                foreach (var x in Xs)
                {
                    hash = hash * 31 + (x?.GetHashCode() ?? 0);
                }
                return hash;
            }
        }
    }
#pragma warning restore
}
