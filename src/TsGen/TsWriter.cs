using System.IO;

namespace TsGen
{
    public static class TsWriter
    {
        public static string Generate(
            TsTypeFactory factory,
            TsFile tsFile,
            TsWriterSettings? settings = null
        )
        {
            using var sw = new StringWriter();
            WriteTo(factory, tsFile, sw, settings);
            return sw.ToString();
        }

        public static void WriteFile(
            TsTypeFactory factory,
            TsFile tsFile,
            string path,
            TsWriterSettings? settings = null
        )
        {
            using var tw = File.CreateText(path);
            WriteTo(factory, tsFile, tw, settings);
        }

        public static void WriteTo(
            TsTypeFactory factory,
            TsFile tsFile,
            TextWriter tw,
            TsWriterSettings? settings = null
        )
        {
            settings ??= new TsWriterSettings();
            // var typesDict = new TsTypesDictionary(tsFile.RootNamespace);
            var writer = new TsClientWriter(factory, tw, false) { Settings = settings };
            writer.Generate(tsFile);
        }

#pragma warning disable CA1861 // Avoid constant arrays as arguments
        public static void SetAngularJsSettings(TsWriterSettings tsWriterSettings)
        {
            tsWriterSettings.ReturnType = new TsTypeName(
                new TsName("IPromise"),
                new[] { new TsGenericParameterTypeName("T") }
            );
            tsWriterSettings.Imports.Add(new TsNamesImport(new[] { "IPromise" }, "angular"));
            tsWriterSettings.MapMethod = TsMapMethod.PromiseThen;
        }

        public static void SetAngularSettings(TsWriterSettings tsWriterSettings)
        {
            tsWriterSettings.ReturnType = new TsTypeName(
                new TsName("Observable"),
                new[] { new TsGenericParameterTypeName("T") }
            );

            tsWriterSettings.Imports.Add(new TsNamesImport(new[] { "Observable" }, "rxjs"));
            tsWriterSettings.Imports.Add(new TsNamesImport(new[] { "map" }, "rxjs/operators"));
            tsWriterSettings.MapMethod = TsMapMethod.ObservablePipeMap;
        }
    }
#pragma warning restore CA1861 // Avoid constant arrays as arguments
}
