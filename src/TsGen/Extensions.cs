using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TsGen
{
    public static class MyEnumerableExtensions
    {
        public static IReadOnlyList<T> AsReadOnly<T>(this IEnumerable<T> source)
        {
            return (source as IReadOnlyList<T>) ?? source.ToList();
        }

        public static ISet<T> ToSet<T>(this IEnumerable<T> source)
        {
            return new HashSet<T>(source);
        }
    }

    public static class MemberInfoExtensions
    {
        public static bool HasAttributeWithName(this MemberInfo mi, string name)
        {
            return HasAttributeWithName(mi.CustomAttributes, name);
        }

        public static bool HasAttributeWithName(this ParameterInfo pi, string name)
        {
            return HasAttributeWithName(pi.CustomAttributes, name);
        }

        static bool HasAttributeWithName(IEnumerable<CustomAttributeData> attrs, string name)
        {
            var attrName = name + "Attribute";
            foreach (var attr in attrs)
            {
                if (attr.AttributeType.Name == attrName)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
