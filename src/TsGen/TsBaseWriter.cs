using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace TsGen
{
    public abstract class TsBaseWriter : IndentendWriter
    {
        readonly List<string> _currentNamespace = new();
        public TsWriterSettings Settings { get; set; }

        protected TsBaseWriter(TextWriter writer, bool ownsWriter)
            : base(writer, ownsWriter)
        {
            Settings = new TsWriterSettings();
        }

        public void WriteTsType(ITsType type)
        {
            switch (type)
            {
                case TsTypeName gn:
                    WriteTsTypeName(gn);
                    break;
                case TsPrimitive p:
                    Write(p.Name);
                    break;
                case TsLiteral l:
                    Write("\"");
                    Write(l.Literal);
                    Write("\"");
                    break;
                case TsObject o:
                    WriteTsObject(o);
                    break;
                case TsIndexable i:
                    if (Settings.Readonly)
                    {
                        Write("{ readonly [key: ");
                    }
                    else
                    {
                        Write("{ [key: ");
                    }
                    WriteTsType(i.KeyType);
                    Write("]: ");
                    WriteTsType(i.ValueType);
                    Write(" }");
                    break;
                case TsUnion u:
                    {
                        var delim = "";
                        foreach (var t in u.Types)
                        {
                            Write(delim);
                            WriteTsType(t);
                            delim = " | ";
                        }
                    }
                    break;
                case TsIntersection i:
                    {
                        var delim = "";
                        foreach (var t in i.Types)
                        {
                            Write(delim);
                            WriteTsType(t);
                            delim = " & ";
                        }
                    }
                    break;
                default:
                    throw new NotSupportedException($"Not handled {type.GetType()}");
            }
        }

        void WriteTsName(TsName name)
        {
            var nss = name.Namespc.SkipWhile(
                (n, i) => i < _currentNamespace.Count && _currentNamespace[i] == n
            );

            var delim = "";
            foreach (var ns in nss)
            {
                Write(delim);
                Write(ns);
                delim = ".";
            }
            Write(delim);

            Write(name.Name);
        }

        public void WriteTsTypeName(TsTypeName name)
        {
            if (Settings.Readonly && TsArray.IsArray(name))
            {
                // FIXME hack
                Write("Readonly");
            }
            WriteTsName(name.Name);
            if (name.GenericTypeParameters.Count == 0)
            {
                return;
            }

            Write("<");
            var delim = "";
            foreach (var t in name.GenericTypeParameters)
            {
                Write(delim);
                WriteTsType(t);
                delim = ", ";
            }
            Write(">");
        }

        void WriteTsProperty(TsProperty p, bool isParam = false)
        {
            if (!isParam && Settings.Readonly)
            {
                Write("readonly ");
            }
            Write(p.Name);
            if (p.Optional)
            {
                Write("?");
            }
            Write(": ");
            WriteTsType(p.Type);
        }

        void WriteTsObject(TsObject obj)
        {
            Write("{ ");
            var delim = "";
            foreach (var p in obj.Properties)
            {
                Write(delim);
                WriteTsProperty(p);
                delim = ", ";
            }
            Write(" }");
        }

        void WriteTsTypeDef(TsTypeDef type)
        {
            switch (type)
            {
                case TsInterface tsInter:
                    Write("interface ");
                    WriteTsTypeName(tsInter.Name);
                    var delim = " extends ";
                    foreach (var name in tsInter.Inherits)
                    {
                        Write(delim);
                        delim = ", ";
                        WriteTsTypeName(name);
                    }
                    Write(" {");
                    IncIndent();
                    foreach (var p in tsInter.Props)
                    {
                        WriteLine();
                        WriteTsProperty(p);
                        Write(";");
                    }
                    DecIndent();
                    WriteLine();
                    Write("}");
                    break;
                case TsEnum tsEnum:
                    if (Settings.ConstEnums)
                    {
                        Write("const ");
                    }
                    Write("enum ");
                    WriteTsTypeName(tsEnum.Name);
                    Write(" {");
                    IncIndent();
                    foreach (var en in tsEnum.Entries)
                    {
                        WriteLine();
                        Write(en.Key);
                        Write(" = ");
                        if (Settings.EnumMode == TsEnumMode.String)
                        {
                            Write("\"");
                            Write(en.Key);
                            Write("\"");
                        }
                        else
                        {
                            Write(en.Value.ToString(CultureInfo.InvariantCulture));
                        }
                        Write(",");
                    }
                    DecIndent();
                    WriteLine();
                    Write("}");
                    break;
                case TsAlias tsAlias:
                    Write("type ");
                    WriteTsTypeName(tsAlias.Name);
                    Write(" = ");
                    WriteTsType(tsAlias.Type);
                    Write(";");
                    break;
                default:
                    throw new NotSupportedException($"Not handled {type.GetType()}");
            }
        }

        void WriteTsNamespace(string? name, TsNamespace ns)
        {
            if (name != null)
            {
                WriteLine();
                Write("export namespace ");
                Write(name);
                Write(" {");
                IncIndent();
                WriteLine();

                _currentNamespace.Add(name);
            }

            foreach (var tdf in ns.TypeDefs)
            {
                WriteLine();
                Write("export ");
                WriteTsTypeDef(tdf);
                WriteLine();
            }

            foreach (var tc in ns.TsClasses)
            {
                WriteLine();
                WriteTsClass(tc);
                WriteLine();
            }

            foreach (var tns in ns.Namespaces)
            {
                WriteTsNamespace(tns.Key, tns.Value);
            }

            if (name != null)
            {
                DecIndent();
                WriteLine();
                Write("}");
                WriteLine();

                _currentNamespace.RemoveAt(_currentNamespace.Count - 1);
            }
        }

        protected abstract void WriteTsClass(TsApiClient c);

        protected virtual void OnTsFileStart() { }

        protected virtual void OnTsFileEnd() { }

        // Includes parens (...)
        protected void WriteParams(IEnumerable<TsProperty> ps)
        {
            Write("(");
            var delim = "";
            foreach (var p in ps.OrderBy(p => p.Optional))
            {
                Write(delim);
                WriteTsProperty(p, isParam: true);
                delim = ", ";
            }
            Write(")");
        }

        // Assumes the properties are defined in variables with the same name
        protected void WriteObject(IReadOnlyCollection<TsProperty> ps)
        {
            if (ps.Count == 0)
            {
                Write("{}");
                return;
            }
            Write("{ ");
            var delim = "";
            foreach (var p in ps)
            {
                Write(delim);
                delim = ", ";
                Write(p.Name);
            }
            Write(" }");
        }

        protected void WriteImport(TsImport import)
        {
            if (import is TsStarImport starImport)
            {
                Write("import * as ");
                Write(starImport.Name);
                Write(" from \"");
                Write(starImport.Module);
                Write("\";");
            }
            else if (import is TsNamesImport namesImport)
            {
                Write("import { ");
                var delim = "";
                foreach (var name in namesImport.Names)
                {
                    Write(delim);
                    delim = ", ";
                    Write(name);
                }
                Write(" } from \"");
                Write(namesImport.Module);
                Write("\";");
            }
            else
            {
                throw new NotSupportedException($"Not handled {import.GetType()}");
            }
        }

        public void Generate(TsFile f)
        {
            OnTsFileStart();

            WriteLine();
            Write(
                "export type RequestFunction = (method: string, actionName: string, queryParams: QueryParams, body?: Object | null) => "
            );
            WriteTsType(Settings.ReturnType.Instantiate(new[] { TsPrimitive.Any }));
            Write(";");
            WriteLine();

            Write("export interface QueryParams {");
            IncIndent();
            WriteLine();
            Write("[name: string]: any;");
            DecIndent();
            WriteLine();
            Write("}");
            WriteLine();

            foreach (var ni in f.NamespaceImports)
            {
                WriteImport(ni);
            }

            WriteTsNamespace(null, f.RootNamespace);
            WriteLine();

            OnTsFileEnd();
        }
    }
}
