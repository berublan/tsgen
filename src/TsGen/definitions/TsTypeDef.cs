using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace TsGen
{
    public abstract class TsTypeDef
    {
        protected TsTypeDef(TsTypeFactory factory, TsTypeName name)
        {
            Name = name;
            factory.AddTypeDef(this);
        }

        public TsTypeName Name { get; }
    }

    public class TsEnum : TsTypeDef
    {
        public TsEnum(TsTypeFactory factory, TsTypeName name, IDictionary<string, int> entries)
            : base(factory, name)
        {
            Entries = new ReadOnlyDictionary<string, int>(entries);
        }

        public IReadOnlyDictionary<string, int> Entries { get; }
    }

    public class TsInterface : TsTypeDef
    {
        public TsInterface(
            TsTypeFactory factory,
            TsTypeName name,
            IEnumerable<TsTypeName> inherits,
            IEnumerable<TsProperty> props
        )
            : base(factory, name)
        {
            Props = props.AsReadOnly();
            Inherits = inherits.AsReadOnly();
        }

        public IReadOnlyList<TsProperty> Props { get; }
        public IReadOnlyList<TsTypeName> Inherits { get; }
    }

    public class TsAlias : TsTypeDef
    {
        public TsAlias(TsTypeFactory factory, TsTypeName name, ITsType type)
            : base(factory, name)
        {
            Type = type;
        }

        public ITsType Type { get; }
    }
}
