namespace TsGen
{
    public static class TsArray
    {
        static readonly TsName ArrayName = new("Array");

        public static bool IsArray(this TsTypeName tn)
        {
            return tn.Name.Namespc.Count == 0
                && tn.Name.Name == ArrayName.Name
                && tn.GenericTypeParameters.Count == 1;
        }

        public static ITsType? GetArrayElementType(this TsTypeName tn)
        {
            if (tn.IsArray())
            {
                return tn.GenericTypeParameters[0];
            }
            else
            {
                return null;
            }
        }

        public static TsTypeName Create(ITsType elementType)
        {
            return new TsTypeName(ArrayName, new[] { elementType });
        }
    }
}
