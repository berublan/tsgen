using System;

namespace TsGen
{
    public abstract class TsTypeRewriter
    {
        public virtual ITsType RewriteTsType(ITsType tsType)
        {
            return tsType switch
            {
                TsPrimitive tsPrimitive => RewriteTsPrimitive(tsPrimitive),
                TsLiteral tsLiteral => RewriteTsLiteral(tsLiteral),
                TsUnion tsUnion => RewriteTsUnion(tsUnion),
                TsIntersection tsIntersection => RewriteTsIntersection(tsIntersection),
                TsObject tsObject => RewriteTsObject(tsObject),
                TsIndexable tsIndexable => RewriteTsIndexable(tsIndexable),
                TsGenericParameterTypeName tsGenericParameterTypeName =>
                    RewriteTsGenericParameterTypeName(tsGenericParameterTypeName),
                TsTypeName tsTypeName => RewriteTsTypeName(tsTypeName),
                _ => throw new ArgumentOutOfRangeException(nameof(tsType)),
            };
        }

        public virtual ITsType RewriteTsPrimitive(TsPrimitive tsPrimitive)
        {
            return tsPrimitive;
        }

        public virtual ITsType RewriteTsLiteral(TsLiteral tsLiteral)
        {
            return tsLiteral;
        }

        public virtual ITsType RewriteTsUnion(TsUnion tsUnion)
        {
            return new TsUnion(tsUnion.Types.Map(RewriteTsType));
        }

        public virtual ITsType RewriteTsIntersection(TsIntersection tsIntersection)
        {
            return new TsIntersection(tsIntersection.Types.Map(RewriteTsType));
        }

        public virtual TsProperty RewriteTsProperty(TsProperty tsProperty)
        {
            return new TsProperty(
                tsProperty.Name,
                RewriteTsType(tsProperty.Type),
                tsProperty.Optional
            );
        }

        public virtual ITsType RewriteTsObject(TsObject tsObject)
        {
            return new TsObject(tsObject.Properties.Map(RewriteTsProperty));
        }

        public virtual ITsType RewriteTsIndexable(TsIndexable tsIndexable)
        {
            return new TsIndexable(
                RewriteTsType(tsIndexable.KeyType),
                RewriteTsType(tsIndexable.ValueType)
            );
        }

        public virtual ITsType RewriteTsTypeName(TsTypeName tsTypeName)
        {
            return new TsTypeName(
                tsTypeName.Name,
                tsTypeName.GenericTypeParameters.Map(RewriteTsType)
            );
        }

        public virtual ITsType RewriteTsGenericParameterTypeName(
            TsGenericParameterTypeName tsTypeName
        )
        {
            return tsTypeName;
        }
    }
}
