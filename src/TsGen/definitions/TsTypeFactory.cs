using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TsGen
{
    public class TsTypeFactory
    {
        public class Badge
        {
            Badge() { }
        }

        static readonly Badge _badge = null!;

        readonly Dictionary<TsTypeName, TsTypeDef> _typeDefs;
        public TsNamespace RootNamespace { get; }

        public TsTypeFactory()
        {
            _typeDefs = new Dictionary<TsTypeName, TsTypeDef>();
            RootNamespace = new TsNamespace();
        }

        public void AddTypeDef(TsTypeDef tsTypeDef)
        {
            if (tsTypeDef.Name.GenericTypeParameters.Count > 0 && tsTypeDef.Name.IsConcrete())
            {
                // this feels like a hack
                return;
            }
            Debug.Assert(!_typeDefs.ContainsKey(tsTypeDef.Name));
            _typeDefs.Add(tsTypeDef.Name, tsTypeDef);
            RootNamespace.AddTsTypeDef(_badge, tsTypeDef);
        }

        public void AddTsApiClient(TsApiClient tsApiClient)
        {
            RootNamespace.AddTsApiClient(_badge, tsApiClient);
        }

        public TsInterface Instantiate(
            TsInterface tsInterface,
            IEnumerable<ITsType> genericTypeParameters
        )
        {
            var newTypeName = tsInterface.Name.Instantiate(genericTypeParameters);
            if (_typeDefs.TryGetValue(newTypeName, out var typeDef))
            {
                return (TsInterface)typeDef;
            }

            var replacements = tsInterface
                .Name.GenericTypeParameters.Cast<TsGenericParameterTypeName>()
                .Zip(genericTypeParameters, (generic, concrete) => new { generic, concrete })
                .ToDictionary((x) => x.generic, x => x.concrete);
            var monomorphizer = new Monomorphizer(replacements);
            var newProps = tsInterface.Props.Map(monomorphizer.RewriteTsProperty);
            return new TsInterface(
                this,
                newTypeName,
                // FIMXE: this is wrong if any of the inherited interfaces use any of generic parameters
                tsInterface.Inherits,
                newProps
            );
        }

        public TsInterface? GetOrInstantiateInterface(TsTypeName tsTypeName)
        {
            if (tsTypeName is TsGenericParameterTypeName)
            {
                return null;
            }

            if (!tsTypeName.IsConcrete())
            {
                return null;
            }

            if (_typeDefs.TryGetValue(tsTypeName, out var typeDef))
            {
                return typeDef as TsInterface;
            }

            // TODO better way to locate generic type from instantiated type name
            var genericType = _typeDefs
                .Where(x =>
                    x.Key.Name == tsTypeName.Name
                    && x.Key.GenericTypeParameters.Count == tsTypeName.GenericTypeParameters.Count
                    && !x.Key.IsConcrete()
                )
                .First();
            if (genericType.Value is not TsInterface genericInterface)
            {
                return null;
            }
            var genericTypeName = genericType.Key;

            var replacements = genericTypeName
                .GenericTypeParameters.Cast<TsGenericParameterTypeName>()
                .Zip(
                    tsTypeName.GenericTypeParameters,
                    (generic, concrete) => new { generic, concrete }
                )
                .ToDictionary((x) => x.generic, x => x.concrete);
            var monomorphizer = new Monomorphizer(replacements);
            var newProps = genericInterface.Props.Map(monomorphizer.RewriteTsProperty);
            return new TsInterface(
                this,
                tsTypeName,
                // FIMXE: this is wrong if any of the inherited interfaces use any of generic parameters
                genericInterface.Inherits,
                newProps
            );
        }
    }
}
