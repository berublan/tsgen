using System.Collections.Generic;

namespace TsGen
{
    public abstract class TsImport
    {
        public string Module { get; }

        public TsImport(string module)
        {
            Module = module;
        }
    }

    public class TsStarImport : TsImport
    {
        public string Name { get; }

        public TsStarImport(string name, string module)
            : base(module)
        {
            Name = name;
        }
    }

    public class TsNamesImport : TsImport
    {
        public IReadOnlyList<string> Names { get; }

        public TsNamesImport(IEnumerable<string> names, string module)
            : base(module)
        {
            Names = names.AsReadOnly();
        }
    }
}
