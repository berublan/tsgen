using System.Collections.Generic;
using System.Linq;

namespace TsGen
{
    public class TsFile
    {
        public TsFile(TsNamespace rootNamespace)
        {
            RootNamespace = rootNamespace;
            NamespaceImports = new List<TsImport>();
        }

        public List<TsImport> NamespaceImports { get; }
        public TsNamespace RootNamespace { get; }
    }

    public class TsNamespace
    {
        public TsNamespace()
        {
            _namespaces = new Dictionary<string, TsNamespace>();
            _typeDefs = new List<TsTypeDef>();
            _tsClasses = new List<TsApiClient>();
        }

        readonly List<TsApiClient> _tsClasses;
        public IReadOnlyList<TsApiClient> TsClasses => _tsClasses;

        readonly List<TsTypeDef> _typeDefs;
        public IReadOnlyList<TsTypeDef> TypeDefs => _typeDefs;

        readonly Dictionary<string, TsNamespace> _namespaces;
        public IReadOnlyDictionary<string, TsNamespace> Namespaces => _namespaces;

        public void AddTsTypeDef(TsTypeFactory.Badge _, TsTypeDef typeDef)
        {
            var tns = GetOrCreateNamespace(this, typeDef.Name.Name.Namespc);
            tns._typeDefs.Add(typeDef);
        }

        public void AddTsApiClient(TsTypeFactory.Badge _, TsApiClient tc)
        {
            var tns = GetOrCreateNamespace(this, tc.Name.Name.Namespc);
            tns._tsClasses.Add(tc);
        }

        static TsNamespace GetOrCreateNamespace(TsNamespace root, IEnumerable<string> ns)
        {
            if (!ns.Any())
            {
                return root;
            }

            var name = ns.First();
            if (!root.Namespaces.TryGetValue(name, out var n))
            {
                n = new TsNamespace();
                root._namespaces.Add(name, n);
            }
            return GetOrCreateNamespace(n, ns.Skip(1));
        }
    }
}
