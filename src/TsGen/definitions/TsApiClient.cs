using System.Collections.Generic;

namespace TsGen
{
    public enum HttpMethod
    {
        GET,
        POST,
    }

    public enum TsApiParamKind
    {
        Query,
        Body,
    }

    public class TsApiParam
    {
        public TsApiParam(TsApiParamKind kind, TsProperty param)
        {
            Kind = kind;
            Param = param;
        }

        public TsApiParamKind Kind { get; }
        public TsProperty Param { get; }
    }

    public class TsApiClient
    {
        public TsApiClient(TsTypeFactory factory, TsTypeName name, IEnumerable<TsApiMethod> methods)
        {
            Name = name;
            Methods = methods.AsReadOnly();
            factory.AddTsApiClient(this);
        }

        public TsTypeName Name { get; }
        public IReadOnlyList<TsApiMethod> Methods { get; }
    }

    public class TsApiMethod
    {
        public TsApiMethod(
            string name,
            HttpMethod method,
            IEnumerable<TsApiParam> parameters,
            ITsType returnType
        )
        {
            Name = name;
            Method = method;
            Parameters = parameters.AsReadOnly();
            ReturnType = returnType;
        }

        public string Name { get; }
        public HttpMethod Method { get; }
        public IReadOnlyList<TsApiParam> Parameters { get; }
        public ITsType ReturnType { get; }
    }
}
