using System.Collections.Generic;

namespace TsGen
{
    internal sealed class Monomorphizer : TsTypeRewriter
    {
        private readonly IReadOnlyDictionary<TsGenericParameterTypeName, ITsType> _replacements;

        public Monomorphizer(IReadOnlyDictionary<TsGenericParameterTypeName, ITsType> replacements)
        {
            _replacements = replacements;
        }

        public override ITsType RewriteTsGenericParameterTypeName(
            TsGenericParameterTypeName tsGenericParameterTypeName
        )
        {
            return _replacements[tsGenericParameterTypeName];
        }
    }
}
