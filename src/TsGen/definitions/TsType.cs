using System;
using System.Collections.Generic;
using System.Linq;

namespace TsGen
{
    public interface ITsType { }

    public class TsPrimitive : ITsType
    {
        public string Name { get; }

        protected TsPrimitive(string name)
        {
            Name = name;
        }

        public static readonly TsPrimitive Number = new("number");
        public static readonly TsPrimitive String = new("string");
        public static readonly TsPrimitive Undefined = new("undefined");
        public static readonly TsPrimitive Null = new("null");
        public static readonly TsPrimitive Void = new("void");
        public static readonly TsPrimitive Date = new("Date");
        public static readonly TsPrimitive Boolean = new("boolean");
        public static readonly TsPrimitive Any = new("any");
        public static readonly TsPrimitive Object = new("object");

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override bool Equals(object? obj)
        {
            return obj is TsPrimitive primitive && Name == primitive.Name;
        }
    }

    public class TsLiteral : ITsType
    {
        public TsLiteral(string literal)
        {
            Literal = literal;
        }

        public string Literal { get; }

        public override bool Equals(object? obj)
        {
            return obj is TsLiteral literal && Literal == literal.Literal;
        }

        public override int GetHashCode()
        {
            return Literal.GetHashCode();
        }
    }

    public class TsUnion : ITsType
    {
        public TsUnion(params ITsType[] types)
        {
            Types = types;
        }

        public TsUnion(IEnumerable<ITsType> types)
        {
            Types = types.AsReadOnly();
        }

        public IReadOnlyList<ITsType> Types { get; }

        public override bool Equals(object? obj)
        {
            return obj is TsUnion union && Enumerable.SequenceEqual(Types, union.Types);
        }

        public override int GetHashCode()
        {
            // FIXME use ValueTuple when we can target netstandard2.0 only
            return Tuple.Create(GetType(), Types.SequenceEquatable()).GetHashCode();
        }
    }

    public class TsIntersection : ITsType
    {
        public TsIntersection(params ITsType[] types)
        {
            Types = types;
        }

        public TsIntersection(IEnumerable<ITsType> types)
        {
            Types = types.AsReadOnly();
        }

        public IReadOnlyList<ITsType> Types { get; }

        public override bool Equals(object? obj)
        {
            return obj is TsIntersection intersection
                && Enumerable.SequenceEqual(Types, intersection.Types);
        }

        public override int GetHashCode()
        {
            // FIXME use ValueTuple when we can target netstandard2.0 only
            return Tuple.Create(GetType(), Types.SequenceEquatable()).GetHashCode();
        }
    }

    public class TsProperty
    {
        public TsProperty(string name, ITsType type, bool optional = false)
        {
            Name = name;
            Type = type;
            Optional = optional;
        }

        public string Name { get; }
        public ITsType Type { get; }
        public bool Optional { get; }

        public override bool Equals(object? obj)
        {
            return obj is TsProperty property
                && Name == property.Name
                && Type.Equals(property.Type)
                && Optional == property.Optional;
        }

        public override int GetHashCode()
        {
            // FIXME use ValueTuple when we can target netstandard2.0 only
            return Tuple.Create(Name, Type, Optional).GetHashCode();
        }
    }

    public class TsObject : ITsType
    {
        public TsObject(params TsProperty[] properties)
        {
            Properties = properties;
        }

        public TsObject(IEnumerable<TsProperty> properties)
        {
            Properties = properties.AsReadOnly();
        }

        public IReadOnlyList<TsProperty> Properties { get; }

        public override bool Equals(object? obj)
        {
            return obj is TsObject @object
                && Enumerable.SequenceEqual(Properties, @object.Properties);
        }

        public override int GetHashCode()
        {
            return Properties.SequenceEquatable().GetHashCode();
        }
    }

    public class TsIndexable : ITsType
    {
        public TsIndexable(ITsType keyType, ITsType valueType)
        {
            KeyType = keyType;
            ValueType = valueType;
        }

        public ITsType KeyType { get; }
        public ITsType ValueType { get; }

        public override bool Equals(object? obj)
        {
            return obj is TsIndexable indexable
                && KeyType.Equals(indexable.KeyType)
                && ValueType.Equals(indexable.ValueType);
        }

        public override int GetHashCode()
        {
            // FIXME use ValueTuple when we can target netstandard2.0 only
            return Tuple.Create(KeyType, ValueType).GetHashCode();
        }
    }
}
