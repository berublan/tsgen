using System;
using System.Collections.Generic;
using System.Linq;

namespace TsGen
{
    public class TsName
    {
        public string Name { get; }
        public IReadOnlyList<string> Namespc { get; }

        public TsName(string name, IEnumerable<string>? namespc = null)
        {
            Name = name;
            Namespc = namespc?.AsReadOnly() ?? Array.Empty<string>();
        }

        public override bool Equals(object? obj)
        {
            return obj is TsName name
                && Name == name.Name
                && Enumerable.SequenceEqual(Namespc, name.Namespc);
        }

        public override int GetHashCode()
        {
            // FIXME use ValueTuple when we can target netstandard2.0 only
            return Tuple.Create(Name, Namespc.SequenceEquatable()).GetHashCode();
        }
    }

    public class TsTypeName : ITsType
    {
        public TsName Name { get; set; }
        public IReadOnlyList<ITsType> GenericTypeParameters { get; set; }

        public TsTypeName(TsName name, IEnumerable<ITsType>? genericTypeParameters = null)
        {
            Name = name;
            GenericTypeParameters = genericTypeParameters?.AsReadOnly() ?? Array.Empty<ITsType>();
        }

        public bool IsConcrete()
        {
            foreach (var item in GenericTypeParameters)
            {
                if (item is TsGenericParameterTypeName)
                {
                    return false;
                }
            }
            return true;
        }

        public TsTypeName Instantiate(IEnumerable<ITsType> genericTypeParameters)
        {
            if (GenericTypeParameters.Count == 0)
            {
                throw new InvalidOperationException("GenericTypeParameters.Count == 0");
            }
            var typeParamList = genericTypeParameters.AsReadOnly();
            if (typeParamList.Count != GenericTypeParameters.Count)
            {
#pragma warning disable CA2208 // Instantiate argument exceptions correctly
                throw new ArgumentException(
                    "genericTypeParameters.Count != this.GenericTypeParameters.Count",
                    nameof(genericTypeParameters)
                );
#pragma warning restore CA2208 // Instantiate argument exceptions correctly
            }
            foreach (var x in typeParamList)
            {
                if (x is TsGenericParameterTypeName)
                {
                    throw new ArgumentException(
                        "genericTypeParameters must be concrete types",
                        nameof(genericTypeParameters)
                    );
                }
            }
            return new TsTypeName(Name, typeParamList);
        }

        public override bool Equals(object? obj)
        {
            return obj is TsTypeName name
                && Name == name.Name
                && Enumerable.SequenceEqual(GenericTypeParameters, name.GenericTypeParameters);
        }

        public override int GetHashCode()
        {
            // FIXME use ValueTuple when we can target netstandard2.0 only
            return Tuple.Create(Name, GenericTypeParameters.SequenceEquatable()).GetHashCode();
        }
    }

    public class TsGenericParameterTypeName : TsTypeName
    {
        public TsGenericParameterTypeName(string name)
            : base(new TsName(name)) { }

        public override bool Equals(object? obj)
        {
            return obj is TsGenericParameterTypeName name && Name.Name == name.Name.Name;
        }

        public override int GetHashCode()
        {
            return Name.Name.GetHashCode();
        }
    }
}
