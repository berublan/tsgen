using System.Collections.Generic;

namespace TsGen
{
    public enum TsEnumMode
    {
        String,
        Number,
    }

    public enum TsMapMethod
    {
        PromiseThen,
        ObservablePipeMap,
    }

    public class TsWriterSettings
    {
        public TsWriterSettings()
        {
            EnumMode = TsEnumMode.Number;
            ConstEnums = false;
            ReturnType = new TsTypeName(
                new TsName("Promise"),
                new[] { new TsGenericParameterTypeName("T") }
            );
            Imports = new List<TsImport>();
            MapMethod = TsMapMethod.PromiseThen;
            GenerateClass = false;
            Readonly = false;
        }

        public TsEnumMode EnumMode { get; set; }
        public bool ConstEnums { get; set; }
        public TsTypeName ReturnType { get; set; }
        public IList<TsImport> Imports { get; }
        public TsMapMethod MapMethod { get; set; }
        public bool GenerateClass { get; set; }
        public bool Readonly { get; set; }
    }
}
