using System;
using System.IO;
using System.Linq;

namespace TsGen
{
    public class TsClientWriter : TsBaseWriter
    {
        readonly DecodeMethodWriter _decodeMethodWriter;

        public TsClientWriter(TsTypeFactory factory, TextWriter writer, bool ownsWriter)
            : base(writer, ownsWriter)
        {
            _decodeMethodWriter = new(factory);
        }

        protected override void OnTsFileStart()
        {
            Write("/* eslint-disable */");
            WriteLine();
            Write("// tslint:disable");
            WriteLine();
            Write("// tool generated");

            foreach (var import in Settings.Imports)
            {
                WriteLine();
                WriteImport(import);
            }
        }

        void WriteApiClientInterface(TsApiClient c)
        {
            Write("export interface ");
            WriteTsType(c.Name);
            Write(" {");
            using (Indented())
            {
                foreach (var m in c.Methods)
                {
                    WriteLine();
                    WriteTsMethodSignature(m);
                    Write(",");
                }
            }
            WriteLine();
            Write("}");
        }

        protected override void WriteTsClass(TsApiClient c)
        {
            WriteApiClientInterface(c);
            WriteLine();
            WriteLine();
            Write("export function ");
            WriteTsType(c.Name);
            Write("Factory(requestFunction: RequestFunction): ");
            WriteTsType(c.Name);
            Write(" {");
            using (Indented())
            {
                WriteLine();
                Write("return {");
                using (Indented())
                {
                    foreach (var m in c.Methods)
                    {
                        WriteLine();
                        WriteTsMethod(m);
                        Write(",");
                    }
                }
                WriteLine();
                Write("};");
            }

            WriteLine();
            Write("}");

            if (Settings.GenerateClass)
            {
                WriteLine();
                WriteLine();
                Write("export const ");
                WriteTsType(c.Name);
                Write(" = class {");
                using (Indented())
                {
                    WriteLine();
                    Write("constructor(requestFunction: RequestFunction) {");
                    using (Indented())
                    {
                        WriteLine();
                        Write("Object.assign(this, ");
                        WriteTsType(c.Name);
                        Write("Factory(requestFunction));");
                    }
                    WriteLine();
                    Write("}");
                }

                WriteLine();
                Write("} as unknown as new (requestFunction: RequestFunction) => ");
                WriteTsType(c.Name);
            }
        }

        void WriteRequestParams(TsApiMethod m)
        {
            Write("\"");
            Write(m.Method.ToString());
            Write("\", \"");
            Write(m.Name);
            Write("\"");
            var queryParams = m
                .Parameters.Where(p => p.Kind == TsApiParamKind.Query)
                .Select(p => p.Param)
                .ToList();
            Write(", ");
            WriteObject(queryParams);

            var bodyParam = m
                .Parameters.Where(p => p.Kind == TsApiParamKind.Body)
                .Select(p => p.Param)
                .SingleOrDefault();
            if (bodyParam != null)
            {
                Write(", ");
                Write(bodyParam.Name);
            }
        }

        void WriteTsMethodSignature(TsApiMethod m)
        {
            Write(m.Name);
            Write(": ");
            WriteParams(m.Parameters.Select(p => p.Param));
            var returnType = Settings.ReturnType.Instantiate(new[] { m.ReturnType });
            Write(" => ");
            WriteTsType(returnType);
        }

        void WriteTsMethod(TsApiMethod m)
        {
            Write(m.Name);
            Write(": (");
            var delim = "";
            foreach (var p in m.Parameters)
            {
                Write(delim);
                delim = ", ";
                Write(p.Param.Name);
            }
            Write(") => ");
            using (Indented())
            {
                WriteLine();
                Write("requestFunction(");
                WriteRequestParams(m);
                Write(")");
                if (_decodeMethodWriter.NeedsDecoding(m.ReturnType))
                {
                    switch (Settings.MapMethod)
                    {
                        case TsMapMethod.PromiseThen:
                            Write(".then(");
                            _decodeMethodWriter.WriteDecodeMethodNameOrLambda(this, m.ReturnType);
                            Write(")");
                            break;
                        case TsMapMethod.ObservablePipeMap:
                            Write(".pipe(map(");
                            _decodeMethodWriter.WriteDecodeMethodNameOrLambda(this, m.ReturnType);
                            Write("))");
                            break;
                        default:
                            throw new NotSupportedException($"Not handled {Settings.MapMethod}");
                    }
                }
            }
        }

        protected override void OnTsFileEnd()
        {
            _decodeMethodWriter.WriteDecodeMethods(this);
        }
    }
}
