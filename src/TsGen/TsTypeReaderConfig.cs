using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TsGen
{
    public class TsTypeReaderConfig
    {
        public virtual (Type, NullabilityInfo?) TypeMapper(
            Type type,
            NullabilityInfo? nullabilityInfo
        )
        {
            return (type, nullabilityInfo);
        }

        public virtual IEnumerable<string> NamespaceMapper(IEnumerable<string> ns)
        {
            return ns;
        }

        public virtual string PropertyNameMapper(string name)
        {
            return name;
        }

        public virtual bool IsApiClass(Type type)
        {
            return type.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase)
                || (
                    type.IsInterface
                    && type.Name.EndsWith("Service", StringComparison.OrdinalIgnoreCase)
                );
        }

        public virtual HttpMethod GetHttpMethod(MethodInfo mi)
        {
            return HttpMethod.POST;
        }
    }

    public class WebApiTsTypeReaderConfig : TsTypeReaderConfig
    {
        readonly bool _ignoreNamespace;
        readonly bool _lowercasePropertyNames;
        readonly Func<Type, NullabilityInfo?, (Type, NullabilityInfo?)>? _typeMapper;

        public WebApiTsTypeReaderConfig(
            bool ignoreNamespace,
            bool lowercasePropertyNames,
            Func<Type, NullabilityInfo?, (Type, NullabilityInfo?)>? typeMapper = null
        )
        {
            _ignoreNamespace = ignoreNamespace;
            _lowercasePropertyNames = lowercasePropertyNames;
            _typeMapper = typeMapper;
        }

        public override (Type, NullabilityInfo?) TypeMapper(
            Type type,
            NullabilityInfo? nullabilityInfo
        )
        {
            if (_typeMapper != null)
            {
                return _typeMapper(type, nullabilityInfo);
            }
            else
            {
                return (type, nullabilityInfo);
            }
        }

        public override IEnumerable<string> NamespaceMapper(IEnumerable<string> ns)
        {
            if (_ignoreNamespace)
            {
                return Enumerable.Empty<string>();
            }
            else
            {
                return ns;
            }
        }

        public override string PropertyNameMapper(string name)
        {
            if (_lowercasePropertyNames)
            {
                return Util.LowerCamelCasePropertyName(name);
            }
            else
            {
                return name;
            }
        }

        static bool IsMvcController(Type type)
        {
            return Util.AnyBaseType(type, t => t.FullName == "System.Web.Mvc.Controller");
        }

        public override HttpMethod GetHttpMethod(MethodInfo mi)
        {
            foreach (var att in mi.CustomAttributes)
            {
                var name = att.AttributeType.Name;
                if (name.StartsWith("HttpGet", StringComparison.OrdinalIgnoreCase))
                {
                    return HttpMethod.GET;
                }
                else if (name.StartsWith("HttpPost", StringComparison.OrdinalIgnoreCase))
                {
                    return HttpMethod.POST;
                }
            }

            // HACK, because MVC doesn't follow this convention
            if (!IsMvcController(mi.DeclaringType!))
            {
                if (mi.Name.StartsWith("get", StringComparison.OrdinalIgnoreCase))
                {
                    return HttpMethod.GET;
                }
            }

            return HttpMethod.POST;
        }
    }
}
