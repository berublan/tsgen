using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace TsGen
{
    public class TsTypeReader
    {
        const BindingFlags DefaultBindingFlags =
            BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly;
        public TsTypeFactory Factory { get; } = new TsTypeFactory();

        readonly Dictionary<Type, TsTypeName> _seenTypes = new();

        readonly TsTypeReaderConfig _config;

        readonly NullabilityInfoContext _nullabilityContext = new();

        readonly Dictionary<Type, TsAlias> _tsAliases = new();

        static class AttrNames
        {
            public const string CanBeNull = "CanBeNull";
            public const string FromUri = "FromUri";
            public const string FromBody = "FromBody";
            public const string FromQuery = "FromQuery";
        }

        public TsTypeReader(TsTypeReaderConfig config)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));
        }

        TsName GetName(Type type)
        {
            var name = type.Name;
            if (type.IsGenericType)
            {
                name = name.Remove(name.IndexOf("`", StringComparison.OrdinalIgnoreCase));
            }

            var ns =
                type.Namespace == null
                    ? Enumerable.Empty<string>()
                    : _config.NamespaceMapper(type.Namespace.Split(Type.Delimiter));
            return new TsName(name, ns);
        }

        TsAlias GetOrCreateAlias(Type type, ITsType tsType)
        {
            if (type.IsGenericType)
            {
                throw new InvalidOperationException("Creating generic aliases not supported");
            }

            if (_tsAliases.TryGetValue(type, out var tsAlias))
            {
                if (!tsAlias.Type.Equals(tsType))
                {
                    throw new InvalidOperationException(
                        "Trying to create alias with different ts type"
                    );
                }
            }
            else
            {
                tsAlias = new TsAlias(Factory, new TsTypeName(GetName(type), null), tsType);
                _tsAliases.Add(type, tsAlias);
            }
            return tsAlias;
        }

        static bool ApiExplorerIgnored(IEnumerable<Attribute> attrs)
        {
            var attr = attrs
                .Where(x => x.GetType().Name == "ApiExplorerSettingsAttribute")
                .FirstOrDefault();

            if (attr != null)
            {
                var ignoreApiPi = attr.GetType().GetProperty("IgnoreApi");
                if (ignoreApiPi != null)
                {
                    var val = ignoreApiPi.GetValue(attr) as bool?;
                    if (val.HasValue)
                    {
                        return val.Value;
                    }
                }
            }

            return false;
        }

        static TsUnion MakeNullable(ITsType t)
        {
            if (t is TsUnion tu)
            {
                if (tu.Types.Contains(TsPrimitive.Null))
                {
                    return tu;
                }
                var types = new List<ITsType>(tu.Types.Count + 1);
                types.AddRange(tu.Types);
                types.Add(TsPrimitive.Null);
                return new TsUnion(types);
            }
            return new TsUnion(t, TsPrimitive.Null);
        }

        TsProperty GenProperty(PropertyInfo pi)
        {
            var tsType = Generate(pi.PropertyType, _nullabilityContext.Create(pi));
            if (pi.HasAttributeWithName(AttrNames.CanBeNull))
            {
                tsType = MakeNullable(tsType);
            }

            return new TsProperty(_config.PropertyNameMapper(pi.Name), tsType);
        }

        TsApiMethod ToTsApiMethod(MethodInfo m)
        {
            var retType = m.ReturnType;
            var nullabilityInfo = _nullabilityContext.Create(m.ReturnParameter);
            if (
                retType.IsGenericType
                // TODO incomplete, what about ValueTask<> for example?
                && retType.GetGenericTypeDefinition() == typeof(Task<>)
            )
            {
                retType = retType.GenericTypeArguments[0];
                nullabilityInfo = nullabilityInfo?.GenericTypeArguments.ElementAtOrDefault(0);
            }
            else if (retType == typeof(Task))
            {
                retType = typeof(void);
            }

            var retTsType = Generate(retType, nullabilityInfo);
            if (m.HasAttributeWithName(AttrNames.CanBeNull))
            {
                retTsType = MakeNullable(retTsType);
            }
            var httpMethod = _config.GetHttpMethod(m);
            var pis = m.GetParameters();
            var pas = new List<TsApiParam>(pis.Length);
            for (var i = 0; i < pis.Length; i++)
            {
                var pi = pis[i];

                var tsType = Generate(pi);

                TsApiParamKind kind;
                if (
                    httpMethod == HttpMethod.GET
                    || pi.HasAttributeWithName(AttrNames.FromUri)
                    || pi.HasAttributeWithName(AttrNames.FromQuery)
                )
                {
                    kind = TsApiParamKind.Query;
                }
                else
                {
                    // POST
                    if (pi.HasAttributeWithName(AttrNames.FromBody))
                    {
                        kind = TsApiParamKind.Body;
                    }
                    else if (tsType is not TsPrimitive && i == (pis.Length - 1))
                    {
                        kind = TsApiParamKind.Body;
                    }
                    else
                    {
                        kind = TsApiParamKind.Query;
                    }
                }

                pas.Add(new TsApiParam(kind, new TsProperty(pi.Name!, tsType)));
            }

            if (pas.Where(x => x.Kind == TsApiParamKind.Body).Count() > 1)
            {
                throw new InvalidOperationException("There can only be 1 Body parameter");
            }

            return new TsApiMethod(m.Name, httpMethod, pas, retTsType);
        }

        static bool IsAssignableToGenericType(Type givenType, Type genericType)
        {
            while (true)
            {
                var interfaceTypes = givenType.GetInterfaces();

                foreach (var it in interfaceTypes)
                {
                    if (it.IsGenericType && it.GetGenericTypeDefinition() == genericType)
                    {
                        return true;
                    }
                }

                if (givenType.IsGenericType && givenType.GetGenericTypeDefinition() == genericType)
                {
                    return true;
                }

                var baseType = givenType.BaseType;
                if (baseType == null)
                {
                    return false;
                }

                givenType = baseType;
            }
        }

        static bool ImplementsByName(Type type, string baseTypeName)
        {
            if (type.IsGenericType)
            {
                return false;
            }

            while (true)
            {
                foreach (var it in type.GetInterfaces())
                {
                    if (it.FullName == baseTypeName)
                    {
                        return true;
                    }
                }

                if (type.FullName == baseTypeName)
                {
                    return true;
                }

                var baseType = type.BaseType;
                if (baseType == null)
                {
                    return false;
                }

                type = baseType;
            }
        }

        ITsType GenObject(Type type, NullabilityInfo? nullabilityInfo)
        {
            var nullableUnderying = Nullable.GetUnderlyingType(type);
            if (nullableUnderying != null)
            {
                return MakeNullable(Generate(nullableUnderying, nullabilityInfo));
            }
            else if (typeof(IEnumerable).IsAssignableFrom(type))
            {
                if (type.IsGenericType && type.GenericTypeArguments.Length == 1)
                {
                    return TsArray.Create(
                        Generate(
                            type.GenericTypeArguments[0],
                            nullabilityInfo?.GenericTypeArguments[0]
                        )
                    );
                }
                else if (type.IsArray)
                {
                    return TsArray.Create(
                        Generate(type.GetElementType()!, nullabilityInfo?.ElementType)
                    );
                }
                else if (IsAssignableToGenericType(type, typeof(IReadOnlyDictionary<,>)))
                {
                    return new TsIndexable(
                        Generate(
                            type.GenericTypeArguments[0],
                            nullabilityInfo?.GenericTypeArguments[0]
                        ),
                        Generate(
                            type.GenericTypeArguments[1],
                            nullabilityInfo?.GenericTypeArguments.ElementAtOrDefault(1)
                        )
                    );
                }
                else
                {
                    Console.Error.WriteLine($"Don't know how to handle \"{type.Name}\"");
                    return TsPrimitive.Any;
                }
            }
            else if (type.IsGenericParameter)
            {
                return new TsGenericParameterTypeName(type.Name);
            }
            else if (type.IsGenericType)
            {
                var genType = type.GetGenericTypeDefinition();
                if (!_seenTypes.TryGetValue(genType, out var tg))
                {
                    var name = GetName(genType);
                    var genTypeParams = genType
                        .GetGenericArguments()
                        .Select(x => new TsGenericParameterTypeName(x.Name));
                    tg = new TsTypeName(name, genTypeParams);
                    _seenTypes.Add(genType, tg);

                    var inherits = GetInheritsTypes(genType, nullabilityInfo);

                    var pis = genType.GetProperties(DefaultBindingFlags);
                    var tsProps = pis.Select(GenProperty);
                    var tsType = new TsInterface(Factory, tg, inherits, tsProps);
                }

                var tsTypeParams = type.GetGenericArguments()
                    .Select(
                        (t, i) =>
                            Generate(t, nullabilityInfo?.GenericTypeArguments.ElementAtOrDefault(i))
                    );
                return new TsTypeName(tg.Name, tsTypeParams);
            }
            else
            {
                //it's not generic
                if (_seenTypes.TryGetValue(type, out var tn))
                {
                    return tn;
                }

                if (ImplementsByName(type, "Microsoft.AspNetCore.Mvc.IActionResult"))
                {
                    return TsPrimitive.Any;
                }

                if (type.IsGenericParameter)
                {
                    return new TsGenericParameterTypeName(type.Name);
                }
                tn = new TsTypeName(GetName(type));
                _seenTypes.Add(type, tn);

                //class with methods
                if (_config.IsApiClass(type))
                {
                    if (ApiExplorerIgnored(type.GetCustomAttributes()))
                    {
                        // FIXME: avoid generating anything for this type
                        return TsPrimitive.Void;
                    }

                    var mis = type.GetMethods(DefaultBindingFlags)
                        .Where(mi => !ApiExplorerIgnored(mi.GetCustomAttributes()));
                    var ms = mis.Select(ToTsApiMethod);
                    var tsClass = new TsApiClient(Factory, tn, ms);
                    return tn;
                }
                else
                {
                    // dto like object
                    var pis = type.GetProperties(DefaultBindingFlags);
                    var tsProps = pis.Select(GenProperty);

                    var inherits = GetInheritsTypes(type, nullabilityInfo);

                    var tsType = new TsInterface(Factory, tn, inherits, tsProps);
                    return tn;
                }
            }
        }

        List<TsTypeName> GetInheritsTypes(Type type, NullabilityInfo? nullabilityInfo)
        {
            var inheritsTypes = new List<Type>();
            if (type.BaseType != null && type.BaseType != typeof(object))
            {
                inheritsTypes.Add(type.BaseType);
            }
            foreach (var it in type.GetInterfaces())
            {
                if (it.Namespace == "System" || it.Namespace == "System.Collections.Generic")
                {
                    // a more robust way to handle this?
                    continue;
                }
                inheritsTypes.Add(it);
            }

            return inheritsTypes
                .Select(t =>
                {
                    var baseTsType = Generate(t, nullabilityInfo);
                    if (baseTsType is not TsTypeName baseTsTypename)
                    {
                        throw new InvalidOperationException("internal error");
                    }
                    return baseTsTypename;
                })
                .ToList();
        }

        TsTypeName GenEnum(Type type)
        {
            Debug.Assert(type.IsEnum);

            if (_seenTypes.TryGetValue(type, out var tn))
            {
                return tn;
            }
            else
            {
                tn = new TsTypeName(GetName(type));
                _seenTypes.Add(type, tn);
                var names = Enum.GetNames(type);
                var values = Enum.GetValues(type);
                var entries = new Dictionary<string, int>(names.Length);
                var i = 0;
                foreach (var val in values)
                {
                    entries.Add(names[i], Convert.ToInt32(val, CultureInfo.InvariantCulture));
                    i++;
                }

                _ = new TsEnum(Factory, tn, entries);
                return tn;
            }
        }

        public void Read(Type type)
        {
            if (type.GetCustomAttribute<CompilerGeneratedAttribute>() != null)
            {
                // compiler generate type
                return;
            }

            Generate(type, null);
        }

        ITsType Generate(ParameterInfo pi)
        {
            var t = Generate(pi.ParameterType, _nullabilityContext.Create(pi));
            if (pi.HasAttributeWithName(AttrNames.CanBeNull))
            {
                return MakeNullable(t);
            }
            return t;
        }

        ITsType GenerateInner(Type type, NullabilityInfo? nullabilityInfo)
        {
            if (type.IsEnum)
            {
                return GenEnum(type);
            }
            // special cases
            else if (type == typeof(void))
            {
                return TsPrimitive.Void;
            }
            else if (type == typeof(DateTimeOffset))
            {
                return TsPrimitive.Date;
            }
            else if (type == typeof(Guid))
            {
                return GetOrCreateAlias(type, TsPrimitive.String).Name;
            }
            else if (type == typeof(TimeSpan))
            {
                return GetOrCreateAlias(type, TsPrimitive.String).Name;
            }

            return Type.GetTypeCode(type) switch
            {
                TypeCode.Empty or TypeCode.DBNull => TsPrimitive.Null,
                TypeCode.Object => GenObject(type, nullabilityInfo),
                TypeCode.Boolean => TsPrimitive.Boolean,
                TypeCode.String or TypeCode.Char => TsPrimitive.String,
                TypeCode.SByte
                or TypeCode.Byte
                or TypeCode.Int16
                or TypeCode.UInt16
                or TypeCode.Int32
                or TypeCode.UInt32
                or TypeCode.Int64
                or TypeCode.UInt64
                or TypeCode.Single
                or TypeCode.Double
                or TypeCode.Decimal => TsPrimitive.Number,
                TypeCode.DateTime => TsPrimitive.Date,
                _ => throw new NotSupportedException(),
            };
        }

        ITsType Generate(Type type, NullabilityInfo? nullabilityInfo)
        {
            (type, nullabilityInfo) = _config.TypeMapper(type, nullabilityInfo);
            var t = GenerateInner(type, nullabilityInfo);
            if (nullabilityInfo?.ReadState == NullabilityState.Nullable)
            {
                return MakeNullable(t);
            }
            return t;
        }
    }
}
